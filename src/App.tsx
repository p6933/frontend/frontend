import React, { useContext, useEffect } from "react";
import {
  BrowserRouter as Router,
  Routes as Switch,
  Route,
} from "react-router-dom";
import LoadingSpinner from "./components/atoms/LoadingSpinner/LoadingSpinner";
import Footer from "./components/organisms/Footer/Footer";
import Navbar from "./components/organisms/navbar/Navbar";
import Flex from "./components/organisms/wrapper/Flex";
import Home from "./pages/Home";

//Aos
import "aos/dist/aos.css";
import Aos from "aos";
import useToken from "./hooks/useToken";
import { AuthContext } from "./components/providers/AuthContextProvider";
import SignIn from "./components/organisms/signin/SignIn";

// lazy loading
const LazyAboutPage: any = React.lazy(() => import("./pages/AboutPage"));
const LazyHome: any = React.lazy(() => import("./pages/Home"));
const LazyAssiduitePage: any = React.lazy(
  () => import("./pages/AssiduitePage")
);
const LazyAssistancePage: any = React.lazy(
  () => import("./pages/AssistancePage")
);
const LazyCalendarPage: any = React.lazy(() => import("./pages/CalendarPage"));
const LazyErrorPage: any = React.lazy(() => import("./pages/ErrorPage"));
const LazyNotesPage: any = React.lazy(() => import("./pages/NotesPage"));
const LazyProfileSettings: any = React.lazy(
  () => import("./pages/ProfileSettings")
);
const LazySigninAdminPage: any = React.lazy(
  () => import("./pages/SigninAdminPage")
);
const LazyAdminHomePage: any = React.lazy(() => import("./pages/AdminHome"));
const LazyStudentInterface: any = React.lazy(
  () => import("./pages/StudentInterface")
);
const LazyUserListAdminPage: any = React.lazy(
  () => import("./pages/UserListAdminPage")
);
const LazyLessonsAdminPage: any = React.lazy(
  () => import("./pages/AdminLessonsPage/AdminLessonPage")
);
const LazyAdminNotesPage: any = React.lazy(
  () => import("./pages/AdminNotesPage/AdminNotesPage")
);
const routes = (
  <>
    <Route index element={<SignIn />} />
    <Route
      path="/about"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAboutPage />
        </React.Suspense>
      }
    />
    <Route
      path="/signin/admin"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazySigninAdminPage />
        </React.Suspense>
      }
    />
    <Route
      path="/*"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyErrorPage />
        </React.Suspense>
      }
    />
  </>
);

const routesAdmin = (
  <>
    <Route
      path="/admin/dashboard"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAdminHomePage />
        </React.Suspense>
      }
    />
    <Route
      path="/admin/userlist"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyUserListAdminPage />
        </React.Suspense>
      }
    />
    <Route
      path="/admin/lessons"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyLessonsAdminPage />
        </React.Suspense>
      }
    />
    <Route
      path="/admin/notes"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAdminNotesPage />
        </React.Suspense>
      }
    />

    <Route
      path="/about"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAboutPage />
        </React.Suspense>
      }
    />
    <Route
      path="/calendar"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyCalendarPage />
        </React.Suspense>
      }
    />
    <Route
      path="/profile"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyProfileSettings />
        </React.Suspense>
      }
    />
    <Route
      path="/assistance"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAssistancePage />
        </React.Suspense>
      }
    />
    <Route
      path="/*"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyErrorPage />
        </React.Suspense>
      }
    />
  </>
);

const routesStudent = (
  <>
    <Route
      path="/about"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAboutPage />
        </React.Suspense>
      }
    />

    <Route
      path="/student/home"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyHome />
        </React.Suspense>
      }
    />
    <Route
      path="/calendar"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyCalendarPage />
        </React.Suspense>
      }
    />
    <Route
      path="/profile"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyProfileSettings />
        </React.Suspense>
      }
    />
    <Route
      path="/student/dashboard"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyStudentInterface />
        </React.Suspense>
      }
    />
    <Route
      path="/student/notes"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyNotesPage />
        </React.Suspense>
      }
    />
    <Route
      path="/student/assiduite"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAssiduitePage />
        </React.Suspense>
      }
    />
    <Route
      path="/assistance"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyAssistancePage />
        </React.Suspense>
      }
    />
    <Route
      path="/*"
      element={
        <React.Suspense fallback={<LoadingSpinner />}>
          <LazyErrorPage />
        </React.Suspense>
      }
    />
  </>
);

function App() {
  const { tokenProfileInfos, tokenId, tokenRole, token } = useToken();
  const authContext = useContext(AuthContext);

  const getCurrentTokenProfile = () => {
    if (token) {
      authContext.dispatch({
        type: "LOGIN",
        payload: {
          isLoggedIn: true,
          currentUser: tokenProfileInfos(),
          userId: tokenId(),
          userType: tokenRole(),
        },
      });
    }
  };
  // initialize aos animations
  useEffect(() => {
    Aos.init({ duration: 1000 });
    getCurrentTokenProfile();
  }, []);

  const uType = authContext.state.userType;
  const isLogged = authContext.state.isLoggedIn;

  return (
    <div style={{ position: "relative", minHeight: "100vh" }}>
      <Router>
        <Navbar />
        <Switch>
          {isLogged && uType === "ADMIN" && routesAdmin}
          {isLogged && uType === "STUDENT" && routesStudent}
          {!isLogged && routes}
        </Switch>
        {/* empty flex for space between content and footer */}
        <Flex height="80px" />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
