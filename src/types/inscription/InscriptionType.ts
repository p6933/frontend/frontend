import { Presence } from "../presence/PresenceType";
import { SchoolClass } from "../schoolClass/SchoolClassType";
import { Student } from "../student/StudentType";

export type Inscription = {
  id?: number;
  startDate?: Date;
  endDate?: Date;
  schoolClass?: SchoolClass;
  student?: Student;
  presences?: Presence[];
};
