import { Evaluation } from "../evaluations/EvaluationType";
import { Student } from "../student/StudentType";

export type Score = {
  id?: number;
  isAbsent?: boolean;
  evaluation?: Evaluation;
  score?: number;
  student?: Student;
};
