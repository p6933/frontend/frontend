import { Teaching } from "../teaching/TeachingType";
import { User } from "../user/UserType";

export type Teacher = {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  profilePicture?: string;
  user?: User;
  teachings?: Teaching[];
};
