import { Evaluation } from "../evaluations/EvaluationType";
import { Inscription } from "../inscription/InscriptionType";
import { Lesson } from "../lesson/LessonType";

export type SchoolClass = {
  id?: number;
  name?: string;
  description?: string;
  inscriptions?: Inscription[];
  lessons?: Lesson[];
  evaluations?: Evaluation[];
};
