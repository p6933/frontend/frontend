export enum EnumStudentInfosOptions {
  avatar = "profilePicture",
  lastName = "lastName",
  firstName = "firstName",
  email = "email",
  birthdate = "birthdate",
  phone = "phone",
}
