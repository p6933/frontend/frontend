import { Evaluation } from "../evaluations/EvaluationType";
import { Lesson } from "../lesson/LessonType";
import { Subject } from "../subject/SubjectType";
import { Teacher } from "../teacher/TeacherType";

export type Teaching = {
  id?: number;
  startDate?: Date;
  endDate?: Date;
  teacher?: Teacher;
  subject?: Subject;
  lessons?: Lesson[];
  evaluations?: Evaluation[];
};
