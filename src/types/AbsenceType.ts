export type Absence = { subject: string; startTime: Date; intervenant: string };
