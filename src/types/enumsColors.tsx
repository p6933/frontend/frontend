export enum EnumColors {
  primary = "#9166FF",
  secondary = "#DBB160",
  light = "#fff",
  grey = "#808080",
  dark = "#000",
  error = '#bb0b0b'
}
