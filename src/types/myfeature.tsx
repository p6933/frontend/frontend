export type MyFeature = {
  appname: string;
  src: any;
  alerts: {
    alertTitle: string;
    alertCount: number;
  };
  link: string
};
