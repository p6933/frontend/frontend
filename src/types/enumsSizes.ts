export enum EnumTextSize {
  fsXxs = "10px",
  fsXs = "12px",
  fsNormal = "14px",
  fsMedium = "18px",
  fsBig = "22px",
  fsXl = "26px",
  fsXxl = "30px",
  fsMax = "50px",
  fsBigger = "70px",
  fsGiant = "200px",
}
