import { Presence } from "../presence/PresenceType";
import { SchoolClass } from "../schoolClass/SchoolClassType";
import { Subject } from "../subject/SubjectType";
import { Teaching } from "../teaching/TeachingType";

export type Lesson = {
  id?: number;
  startDate?: Date;
  endDate?: Date;
  schoolClass?: SchoolClass;
  subject?: Subject;
  teaching?: Teaching;
  classRoom?: {
    id: number;
    name: string;
    description: string;
  };
  presences?: Presence[];
};
