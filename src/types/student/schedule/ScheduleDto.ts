export type Schedule = {
  lessonStartDate: Date;
  lessonEndDate: Date;
  subjectName: string;
  subjectDescription: string;
  teacherLastName: string;
  teacherFirstName: string;
  classRoomName: string;
  classRoomDesc: string;
};
