export type SchedulePayload = {
  startDate: Date;
  endDate: Date;
  email: string;
  schoolYear: number;
};
