export type StudentInfosDto = {
  email: string;
  profilePicture: string | undefined | null;
  firstName: string;
  lastName: string;
  birthdate: Date;
  address: string;
  phone: number;
};
