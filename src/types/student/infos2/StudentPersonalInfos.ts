export type StudentPersonalInfos = {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  profilePicture?: string;
  birthDate?: Date;
  phone?: string;
};
