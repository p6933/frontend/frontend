export type StudentNotesPayload = {
  subject?: string;
  notes?: {
    id?: number;
    score?: number;
    isAbsent?: boolean;
  }[];
};
