export type StudentPresenceInfo = {
  moduleName?: string;
  startDate?: Date;
  endDate?: Date;
  delay?: number;
};
