import { StudentNotesPayload } from "./StudentNotesPayload";
import { StudentPresenceInfosPayload } from "./StudentPresenceInfosPayload";
import { StudentPersonalInfos } from "./StudentPersonalInfos";

export type StudentInfosPayload = {
  studentInfos: {
    studentPersonalInfos: StudentPersonalInfos;
    presence: StudentPresenceInfosPayload;
    studentNotes: StudentNotesPayload;
  };
};
