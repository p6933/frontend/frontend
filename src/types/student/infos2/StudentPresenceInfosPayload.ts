import { StudentPresenceInfo } from "./StudentPresenceInfo";
export type StudentPresenceInfosPayload = StudentPresenceInfo[];
