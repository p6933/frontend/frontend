import { Inscription } from "../inscription/InscriptionType";
import { Score } from "../scores/ScoreType";
import { User } from "../user/UserType";

export type Student = {
  id?: number;
  email?: string;
  password?: string;
  profilePicture?: string;
  user?: User;
  inscriptions?: Inscription[];
  scores?: Score[];
};
