export enum EnumTabNotesOptions {
  create = "CREATE",
  list = "LIST",
  update = "UPDATE",
}
