export type Ticket = {
  id: number;
  subject: string;
  status: "OPEN" | "CLOSED";
};
