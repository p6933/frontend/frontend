import { Inscription } from "../inscription/InscriptionType";
import { Lesson } from "../lesson/LessonType";

export type Presence = {
  id?: number;
  isPresent?: boolean;
  inscription?: Inscription;
  lesson?: Lesson;
};
