import { SchoolClass } from "../schoolClass/SchoolClassType";
import { Score } from "../scores/ScoreType";
import { Subject } from "../subject/SubjectType";
import { Teaching } from "../teaching/TeachingType";

export type Evaluation = {
  id?: number;
  startDate?: Date;
  endDate?: Date;
  schoolClass?: SchoolClass;
  subject?: Subject;
  teachings?: Teaching[];
  scores?: Score[];
};
