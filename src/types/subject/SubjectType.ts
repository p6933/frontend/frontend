import { Evaluation } from "../evaluations/EvaluationType";
import { Lesson } from "../lesson/LessonType";
import { Teaching } from "../teaching/TeachingType";

export type Subject = {
  id?: number;
  name?: string;
  description?: string;
  lessons?: Lesson[];
  evaluations?: Evaluation[];
  teachings?: Teaching[];
};
