export type CreateLessonType = {
  startDate: Date;
  endDate: Date;
  schoolClass: number;
  subject: number;
  teaching: number;
  classRoom: number;
};
