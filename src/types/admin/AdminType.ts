import { User } from "./../user/UserType";
export type Admin = {
  id?: number;
  email?: string;
  password?: string;
  profilePicture?: string;
  user?: User;
};
