export type CreateNoteType = {
  isAbsent: boolean;
  score: number;
  student: number;
  evaluation: number;
};
