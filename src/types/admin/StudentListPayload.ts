export type StudentsListPayload = {
  id?: number;
  firstName?: string;
  lastName?: string;
  schoolClassNames?: (string | undefined)[];
  profilePicture?: string;
  email?: string;
};
