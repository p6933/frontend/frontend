import { CreateLessonType } from "./CreateLessonType";
export type UpdateLessonType = CreateLessonType & {
  id: number;
};
