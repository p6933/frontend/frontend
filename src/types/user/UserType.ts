import { Admin } from "../admin/AdminType";
import { Student } from "../student/StudentType";
import { Teacher } from "../teacher/TeacherType";

export type User = {
  id?: number;
  firstName?: string;
  lastName?: string;
  birthdate?: Date;
  address?: string;
  phone?: string;
  students?: Student[];
  teachers?: Teacher[];
  admins?: Admin[];
};
