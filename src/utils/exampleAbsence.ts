export const exampleAbsence = [
    {
      subject: "Javascript",
      startTime: new Date(2022, 1, 2),
      intervenant: 'John Doe',
    },
    {
      subject: "Nodejs",
      startTime: new Date(2022, 1, 2),
      intervenant: 'Jeanne Doe',
    },
    {
      subject: "Java",
      startTime: new Date(2022, 1, 2),
      intervenant: 'Dummy Doe',
    },
  ];
  