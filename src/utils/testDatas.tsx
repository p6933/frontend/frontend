import ImageAssiduite from "../images/time.jpg";
import ImageCalendar from "../images/calendar2.jpg";
import ImagePInfos from "../images/infos.jpg";
import ImageNotes from "../images/notes.jpg";
import ImageAssist from "../images/assist.jpg";

export const features = [
  {
    appname: "Assiduité",
    src: ImageAssiduite,
    alerts: {
      alertTitle: "Alertes Assiduité",
      alertCount: 2,
    },
    link: '/student/assiduite'
  },
  {
    appname: "Calendrier",
    src: ImageCalendar,
    alerts: {
      alertTitle: "Alertes Calendrier",
      alertCount: 1,
    },
    link: '/calendar'
  },
  {
    appname: "Informations Personelles",
    src: ImagePInfos,
    alerts: {
      alertTitle: "Alertes Infos Personelles",
      alertCount: 2,
    },
    link: '/profile'
  },
  {
    appname: "Notes",
    src: ImageNotes,
    alerts: {
      alertTitle: "Alertes Notes",
      alertCount: 1,
    },
    link: '/student/notes'
  },
  {
    appname: "Assistance",
    src: ImageAssist,
    alerts: {
      alertTitle: "Alertes Assistance",
      alertCount: 0,
    },
    link: '/assistance'
  },
];
