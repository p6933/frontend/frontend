import { Ticket } from "../types/TicketType";

export const exampleTickets: Ticket[] = [
  {
    id: 1,
    subject: "Note projet design",
    status: "OPEN",
  },
  {
    id: 2,
    subject: "Note projet game",
    status: "OPEN",
  },
  {
    id: 3,
    subject: "Absence 26/03/2022",
    status: "CLOSED",
  },
  {
    id: 4,
    subject: "Retard 02/05/2022",
    status: "CLOSED",
  },
];
