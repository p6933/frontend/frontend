export const actionOnKey = (key: string, callback: any) => {
  window.addEventListener("keydown", function (event) {
    if (event.key === key) {
      callback();
    }
  });
};
