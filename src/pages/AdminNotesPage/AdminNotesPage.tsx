import { useState } from "react";
import Spacer from "../../components/atoms/Spacer/Spacer";
import Text from "../../components/atoms/Text/Text";
import AdminHandleNotes from "../../components/organisms/AdminHandleNotes/AdminHandleNotes";
import ScoresList from "../../components/organisms/AdminHandleNotes/NotesList";
import UpdateNoteForm from "../../components/organisms/AdminHandleNotes/UpdateNoteForm";
import TabsNotesOptions from "../../components/organisms/TabsNotesOptions/TabsNotesOptions";
import { EnumColors } from "../../types/enumsColors";
import { EnumTextSize } from "../../types/enumsSizes";
import { EnumTabNotesOptions } from "../../types/EnumTabNotesOption";
import styles from "./admin-notes-page.module.scss";

type Props = {};

const AdminNotesPage = (props: Props) => {
  const [tab, setTab] = useState<EnumTabNotesOptions>(
    EnumTabNotesOptions.create
  );

  const changeTab = (option: EnumTabNotesOptions) => {
    switch (option) {
      case EnumTabNotesOptions.create:
        setTab(EnumTabNotesOptions.create);
        break;

      case EnumTabNotesOptions.list:
        setTab(EnumTabNotesOptions.list);
        break;

      case EnumTabNotesOptions.update:
        setTab(EnumTabNotesOptions.update);
        break;

      default:
        break;
    }
  };
  return (
    <div className={styles.notesPageWrapper}>
      <Text
        size={EnumTextSize.fsMax}
        fontStyle="italic"
        value={"Notes"}
        color={EnumColors.primary}
      />
      <Spacer height={20} />
      <div className={styles.boxNotes}>
        <TabsNotesOptions changeTab={changeTab} tab={tab} />
        {tab === EnumTabNotesOptions.create && <AdminHandleNotes />}
        {tab === EnumTabNotesOptions.list && <ScoresList />}
        {tab === EnumTabNotesOptions.update && <UpdateNoteForm />}
      </div>
    </div>
  );
};

export default AdminNotesPage;
