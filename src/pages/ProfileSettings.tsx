import { useContext, useEffect, useState } from "react";
import { AdminService } from "../api/admin-api";
import { StudentService } from "../api/student-api";
import LoadingSpinner from "../components/atoms/LoadingSpinner/LoadingSpinner";
import PageHeader from "../components/organisms/PageHeader/PageHeader";
import ProfileInfos from "../components/organisms/ProfileInfos/ProfileInfos";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperSplit from "../components/organisms/wrapper/WrapperSplit";
import { AuthContext } from "../components/providers/AuthContextProvider";
import { StudentInfosPayload } from "../types/student/infos/StudentInfosPayload";

const ProfileSettings = () => {
  const [studentInfos, setStudentInfos] = useState<any>();
  const authContext = useContext(AuthContext);

  const sendReq = async () => {
    const email: StudentInfosPayload = {
      email: authContext.state?.currentUser?.email,
    };

    const userId = authContext.state?.userId;
    console.log("infos:", authContext.state);
    console.log("email:", authContext.state?.currentUser?.email);
    let res;
    if (authContext.state.userType === "ADMIN") {
      res = await AdminService.getAdminDetails(userId);
    }
    if (authContext.state.userType === "STUDENT") {
      res = await StudentService.studentInfos(email);
    }
    setStudentInfos(res);
  };

  useEffect(() => {
    sendReq();
  }, []);

  return (
    <>
      <Wrapper>
        <WrapperSplit>
          <PageHeader value="Profile" animated />
          {studentInfos ? (
            <ProfileInfos studentInfos={studentInfos} />
          ) : (
            <LoadingSpinner />
          )}
        </WrapperSplit>
      </Wrapper>
    </>
  );
};

export default ProfileSettings;
