import AboutContent from "../components/organisms/AboutContent/AboutContent";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperFlex from "../components/organisms/wrapper/WrapperFlex";
import PageHeader from "../components/organisms/PageHeader/PageHeader";

const AboutPage = () => {
  return (
    <>
      <Wrapper>
        <WrapperFlex>
          <PageHeader value="About" />
          <AboutContent />
        </WrapperFlex>
      </Wrapper>
    </>
  );
};

export default AboutPage;
