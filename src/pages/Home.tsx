import FeaturesAvailables from "../components/organisms/FeaturesAvailables/FeaturesAvailables";
import HelloUser from "../components/organisms/HelloUser/HelloUser";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperFlex from "../components/organisms/wrapper/WrapperFlex";

const Home = () => {
  return (
    <>
      <Wrapper>
        <WrapperFlex>
          <HelloUser />
          <FeaturesAvailables />
        </WrapperFlex>
      </Wrapper>
    </>
  );
};

export default Home;
