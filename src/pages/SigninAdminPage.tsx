import LoginIllustration from "../components/organisms/LoginIllustration/LoginIllustration";
import SigninAdmin from "../components/organisms/signinAdmin/SigninAdmin";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperSplit from "../components/organisms/wrapper/WrapperSplit";

const SigninAdminPage = () => {
  return (
    <>
      <Wrapper>
        <WrapperSplit>
          <LoginIllustration />
          <SigninAdmin />
        </WrapperSplit>
      </Wrapper>
    </>
  );
};

export default SigninAdminPage;
