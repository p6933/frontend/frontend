import BoxAdmin from "../components/molecules/BoxAdmin/BoxAdmin";
import GridAdmin from "../components/organisms/GridAdmin/GridAdmin";
import HelloUser from "../components/organisms/HelloUser/HelloUser";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperFlex from "../components/organisms/wrapper/WrapperFlex";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import { EnumTextSize } from "../types/enumsSizes";

const AdminHome = () => {
  const navigate = useNavigate();
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const responsiveTextSize = !isTabletOrMobile
    ? EnumTextSize.fsXl
    : EnumTextSize.fsMedium;
  const responsiveLeftGrid = !isTabletOrMobile ? "90px" : "0px";
  return (
    <>
      <Wrapper>
        <WrapperFlex>
          <HelloUser />
          <GridAdmin left={responsiveLeftGrid}>
            <BoxAdmin
              aos="fade-right"
              fontSize={responsiveTextSize}
              title={"Utilisateurs"}
              handleClick={() => navigate("/admin/userlist")}
            />
            <BoxAdmin
              aos="fade-down"
              fontSize={responsiveTextSize}
              title={"Bugs"}
              handleClick={() => navigate("/admin/bugs")}
            />
            <BoxAdmin
              aos="fade-right"
              fontSize={responsiveTextSize}
              title={"Assiduité"}
              handleClick={() => navigate("/admin/assiduite")}
            />
            <BoxAdmin
              aos="fade-left"
              fontSize={responsiveTextSize}
              title={"Planning"}
              handleClick={() => navigate("/calendar")}
            />
            <BoxAdmin
              aos="fade-right"
              fontSize={responsiveTextSize}
              title={"Notes"}
              handleClick={() => navigate("/admin/notes")}
            />
            <BoxAdmin
              aos="fade-left"
              fontSize={responsiveTextSize}
              title={"Assistance"}
              handleClick={() => navigate("/admin/assistance")}
            />
            <BoxAdmin
              aos="fade-up"
              fontSize={responsiveTextSize}
              title={"Cours"}
              handleClick={() => navigate("/admin/lessons")}
            />
          </GridAdmin>
        </WrapperFlex>
      </Wrapper>
    </>
  );
};

export default AdminHome;
