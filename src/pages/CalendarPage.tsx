import Wrapper from "../components/organisms/wrapper/Wrapper";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { useContext, useEffect, useRef, useState } from "react";
import { EnumColors } from "../types/enumsColors";
import ModalCreateEvent from "../components/organisms/ModalCreateEvent/ModalCreateEvent";
import Button from "../components/atoms/Button/Button";
import LoadingSpinner from "../components/atoms/LoadingSpinner/LoadingSpinner";
import { SchedulePayload } from "../types/student/schedule/SchedulePayload";
import { StudentService } from "../api/student-api";
import ModalEvent from "../components/organisms/ModalEvent/ModalEvent";
import { AuthContext } from "../components/providers/AuthContextProvider";
import { SchoolClass } from "../types/schoolClass/SchoolClassType";
import InputSelectOption from "../components/atoms/InputSelectOption/InputSelectOption";
import FormContainer from "../components/organisms/FormContainer/FormContainer";
import { AdminService } from "../api/admin-api";
import { useCreateLessonForm } from "../components/organisms/CreateLessonForm/hooks/useCreateLessonForm";
import Spacer from "../components/atoms/Spacer/Spacer";
import LabelInput from "../components/molecules/LabelInput/LabelInput";
import Text from "../components/atoms/Text/Text";

const CalendarPage = () => {
  const authContext = useContext(AuthContext);
  const { classesList } = useCreateLessonForm();

  const [events, setEvents] = useState<any>();
  const [eventClicked, setEventClicked] = useState<any>();
  const [modalEventView, setModalEventView] = useState<boolean>(false);
  const [modal, setModal] = useState<boolean>(false);
  const [schoolClassCalendar, setSchoolClassCalendar] = useState<any>();

  const getCalendarSubmit = async (formValues: any) => {
    if (authContext.state.userType === "ADMIN") {
      const dummyPayload = {
        startDate: new Date(2021, 10, 1),
        endDate: new Date(2022, 10, 1),
        schoolClass: formValues.schoolClass,
      };

      const schoolClassSchedule = await AdminService.getSchoolClassSchedule(
        dummyPayload
      );

      schoolClassSchedule.forEach((element: any) => {
        element.start = element.lessonStartDate;
        element.end = element.lessonEndDate;
        element.title = element.subjectName;
        element.description = element.classRoomDesc;
      });
      setSchoolClassCalendar(schoolClassSchedule);
    }
  };

  // need to be replace with the current student authenticate
  let dummyPayload: SchedulePayload;
  if (authContext.state.userType === "STUDENT") {
    dummyPayload = {
      startDate: new Date(2021, 10, 1),
      endDate: new Date(2022, 10, 1),
      email: authContext.state.currentUser?.email,
      schoolYear: 2021,
    };
  }

  const fetchSchedule = async () => {
    const schedule = await StudentService.studentSchedule(dummyPayload);

    schedule.forEach((element: any) => {
      element.start = element.lessonStartDate;
      element.end = element.lessonEndDate;
      element.title = element.subjectName;
      element.description = element.subjectDescription;
    });

    setEvents(schedule);
  };

  let calendarRef = useRef(null);

  const closeModalEvent = () => setModalEventView(false);
  const closeModal = () => setModal(false);
  const openModal = () => setModal(true);

  const openModalEvent = (eventValues: any) => {
    setEventClicked(eventValues);
    setModalEventView(true);
  };

  useEffect(() => {
    fetchSchedule();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {authContext.state.userType === "STUDENT" && (
        <Wrapper aos="fade-right">
          {events ? (
            <FullCalendar
              ref={calendarRef}
              plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
              initialView="dayGridMonth"
              headerToolbar={{
                left: "prev next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay",
              }}
              editable={true}
              selectable={true}
              selectMirror={true}
              dayMaxEvents={true}
              weekends={true}
              events={events}
              eventBackgroundColor={EnumColors.primary}
              eventBorderColor={EnumColors.primary}
              eventClick={(data) =>
                openModalEvent(data.event._def.extendedProps)
              }
            />
          ) : (
            <LoadingSpinner />
          )}
        </Wrapper>
      )}
      {authContext.state.userType === "ADMIN" && (
        <Wrapper>
          {authContext.state.userType === "ADMIN" && (
            <Button
              text="Créer un cours"
              width="200px"
              margin=" 30px 0"
              handleClick={openModal}
            />
          )}
          <FormContainer onSubmit={getCalendarSubmit}>
            <Spacer height={20} />
            {classesList && (
              <LabelInput label={<Text value="Classe" fontStyle="italic" />}>
                <Spacer height={20} />
                <InputSelectOption
                  name={"schoolClass"}
                  data={classesList}
                  datatype="object-v1-list"
                />
              </LabelInput>
            )}
            <Spacer height={40} />
            <Button text={"Afficher calendrier"} />
          </FormContainer>

          {schoolClassCalendar && (
            <div data-aos="fade-out">
              <FullCalendar
                ref={calendarRef}
                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                initialView="dayGridMonth"
                headerToolbar={{
                  left: "prev next today",
                  center: "title",
                  right: "dayGridMonth,timeGridWeek,timeGridDay",
                }}
                editable={true}
                selectable={true}
                selectMirror={true}
                dayMaxEvents={true}
                weekends={true}
                events={schoolClassCalendar}
                eventBackgroundColor={EnumColors.primary}
                eventBorderColor={EnumColors.primary}
                eventClick={(data) =>
                  openModalEvent(data.event._def.extendedProps)
                }
              />
            </div>
          )}
        </Wrapper>
      )}
      {modalEventView && (
        <ModalEvent handleClose={closeModalEvent} event={eventClicked} />
      )}
      {/* Only for prof and admin, need a condition with type of user */}
      {modal && (
        <ModalCreateEvent
          handleClose={closeModal}
          setEvents={setEvents}
          events={events}
        />
      )}
    </>
  );
};

export default CalendarPage;
