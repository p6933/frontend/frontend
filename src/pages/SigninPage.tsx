import LoginIllustration from "../components/organisms/LoginIllustration/LoginIllustration";
import SignIn from "../components/organisms/signin/SignIn";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperSplit from "../components/organisms/wrapper/WrapperSplit";

const SigninPage = () => {

  return (
    <>
      <Wrapper>
        <WrapperSplit>
          <LoginIllustration />
          <SignIn />
        </WrapperSplit>
      </Wrapper>
    </>
  );
};

export default SigninPage;
