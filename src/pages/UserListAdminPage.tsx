import { useState } from "react";
import AdminUserList from "../components/organisms/AdminUserList/AdminUserList";
import Wrapper from "../components/organisms/wrapper/Wrapper";
import WrapperFlex from "../components/organisms/wrapper/WrapperFlex";

type Props = {};

const UserListAdminPage = (props: Props) => {
  const [mode, setMode] = useState<"students" | "teachers">("students");
  return (
    <>
      <Wrapper>
        <WrapperFlex>
          <AdminUserList mode={mode} />
        </WrapperFlex>
      </Wrapper>
    </>
  );
};

export default UserListAdminPage;
