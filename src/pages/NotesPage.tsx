import NotesList from '../components/organisms/notes/NotesList'
import Wrapper from '../components/organisms/wrapper/Wrapper'
import PageHeader from '../components/organisms/PageHeader/PageHeader'
import WrapperSplit from '../components/organisms/wrapper/WrapperSplit'
import { exampleNotes } from '../components/organisms/notes/exampleNotes'

const NotesPage = () => {
  

  return (
    <>
        <Wrapper>
          <WrapperSplit>
            <PageHeader value='Notes' animated />
            <NotesList notes={exampleNotes} />
          </WrapperSplit>
        </Wrapper>
    </>
  )
}

export default NotesPage