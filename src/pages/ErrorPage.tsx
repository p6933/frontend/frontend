import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import Button from "../components/atoms/Button/Button";
import Text from "../components/atoms/Text/Text";
import Notfound from "../components/molecules/Notfound/Notfound";
import WrapperCenter from "../components/organisms/wrapper/WrapperCenter";
import { AuthContext } from "../components/providers/AuthContextProvider";
import { EnumTextSize } from "../types/enumsSizes";

const ErrorPage = () => {
  const navigate = useNavigate();
  const authContext = useContext(AuthContext);

  const navigateHome = () => {
    if (authContext.state.userType === "ADMIN") {
      navigate("/admin/dashboard");
    } else if (authContext.state.userType === "STUDENT") {
      navigate("/student/home");
    } else {
      navigate("/");
    }
  };
  return (
    <>
      <WrapperCenter>
        <Notfound />
        <Text
          value="Error"
          size={EnumTextSize.fsMax}
          weight="bold"
          fontStyle="italic"
          margin="0 0 80px 0"
        />
        <Text value="Oups !" size={EnumTextSize.fsBig} />
        <Text
          value="Impossible de trouver votre recherche ..."
          size={EnumTextSize.fsBig}
        />
        <Button
          text="Retour"
          handleClick={navigateHome}
          height="40px"
          margin="10px"
        />
      </WrapperCenter>
    </>
  );
};

export default ErrorPage;
