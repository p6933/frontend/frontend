import AssiduiteList from '../components/organisms/assiduite/AssiduiteList'
import { exampleAbsence } from '../utils/exampleAbsence'
import PageHeader from '../components/organisms/PageHeader/PageHeader'
import Wrapper from '../components/organisms/wrapper/Wrapper'
import WrapperSplit from '../components/organisms/wrapper/WrapperSplit'
import { exampleRetards } from '../utils/exampleRetard'

const AbsencesPage = () => {
  return (
    <>
        <Wrapper>
          <WrapperSplit>
            <PageHeader value='Assiduité' animated />
            <AssiduiteList absences={exampleAbsence} retards={exampleRetards} />
          </WrapperSplit>
        </Wrapper>
    </>
  )
}

export default AbsencesPage