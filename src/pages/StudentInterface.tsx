import Wrapper from "../components/organisms/wrapper/Wrapper";
import PageHeader from "../components/organisms/PageHeader/PageHeader";
import DashboardStudent from "../components/organisms/DashboardStudent/DashboardStudent";
import WrapperSplit from "../components/organisms/wrapper/WrapperSplit";

const StudentInterface = () => {
  return (
    <>
      <Wrapper>
        <WrapperSplit>
          <PageHeader value="Dashboard" />
          <DashboardStudent />
        </WrapperSplit>
      </Wrapper>
    </>
  );
};

export default StudentInterface;
