import { useState } from "react";
import Spacer from "../../components/atoms/Spacer/Spacer";
import Text from "../../components/atoms/Text/Text";
import CreateLessonForm from "../../components/organisms/CreateLessonForm/CreateLessonForm";
import LessonList from "../../components/organisms/CreateLessonForm/LessonList";
import UpdateLessonForm from "../../components/organisms/CreateLessonForm/UpdateLessonForm";
import TabsNotesOptions from "../../components/organisms/TabsNotesOptions/TabsNotesOptions";
import { EnumColors } from "../../types/enumsColors";
import { EnumTextSize } from "../../types/enumsSizes";
import { EnumTabNotesOptions } from "../../types/EnumTabNotesOption";
import styles from "./admin-lesson-page.module.scss";

type Props = {};

const AdminLessonPage = (props: Props) => {
  const [tab, setTab] = useState<EnumTabNotesOptions>(
    EnumTabNotesOptions.create
  );

  const changeTab = (option: EnumTabNotesOptions) => {
    switch (option) {
      case EnumTabNotesOptions.create:
        setTab(EnumTabNotesOptions.create);
        break;

      case EnumTabNotesOptions.list:
        setTab(EnumTabNotesOptions.list);
        break;

      case EnumTabNotesOptions.update:
        setTab(EnumTabNotesOptions.update);
        break;

      default:
        break;
    }
  };
  return (
    <div className={styles.lessonsPageWrapper}>
      <Text
        size={EnumTextSize.fsMax}
        fontStyle="italic"
        value={"Lessons"}
        color={EnumColors.primary}
      />
      <Spacer height={20} />
      <div className={styles.boxLessons}>
        <TabsNotesOptions changeTab={changeTab} tab={tab} />
        {tab === EnumTabNotesOptions.create && <CreateLessonForm />}
        {tab === EnumTabNotesOptions.list && <LessonList />}
        {tab === EnumTabNotesOptions.update && <UpdateLessonForm />}
      </div>
    </div>
  );
};

export default AdminLessonPage;
