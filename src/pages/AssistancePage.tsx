import PageHeader from "../components/organisms/PageHeader/PageHeader"
import Tickets from "../components/organisms/Tickets/Tickets"
import Wrapper from "../components/organisms/wrapper/Wrapper"
import WrapperSplit from "../components/organisms/wrapper/WrapperSplit"
import { exampleTickets } from "../utils/exampleTickets"

const AssistancePage = () => {
  return (
    <>
    <Wrapper>
        <WrapperSplit>
            <PageHeader animated value="Assistance" />
            <Tickets tickets={exampleTickets} />
        </WrapperSplit>
    </Wrapper>
    </>
  )
}

export default AssistancePage