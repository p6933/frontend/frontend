import { render } from "@testing-library/react";
import axios from "axios";
import Toaster from "../components/atoms/Toaster/Toaster";
import { configEnv } from "../environments";
import { LoginType } from "../types/LoginType";

export const TeacherService = {
  async sendLogin(formValues: LoginType) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/teacher/connect`,
        formValues
      );
      render(<Toaster success toastMsg="Connexion réussie !" />);

      console.log(res);
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
};
