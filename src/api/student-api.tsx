import { render } from "@testing-library/react";
import axios from "axios";
import Toaster from "../components/atoms/Toaster/Toaster";
import { configEnv } from "../environments";
import { LoginType } from "../types/LoginType";
import { StudentInfosPayload } from "../types/student/infos/StudentInfosPayload";
import { SchedulePayload } from "../types/student/schedule/SchedulePayload";

export const StudentService = {
  async sendLogin(formValues: LoginType) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/student/connect`,
        formValues
      );
      render(<Toaster success toastMsg="Connexion réussie !" />);
      return res.data;

    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
  async studentSchedule(formValues: SchedulePayload) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/student/schedule`,
        formValues
      );
      return res.data.schedule;
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
  async studentInfos(email: StudentInfosPayload) {
    try {
      const res = await axios.post(`${configEnv.API_URL}/student/info`, email);
      return res.data;
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
};
