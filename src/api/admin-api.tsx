import { render } from "@testing-library/react";
import axios from "axios";
import Toaster from "../components/atoms/Toaster/Toaster";
import { configEnv } from "../environments";
import { CreateLessonType } from "../types/admin/CreateLessonType";
import { CreateNoteType } from "../types/admin/CreateNoteType";
import { UpdateLessonType } from "../types/admin/UpdateLessonType";
import { LoginType } from "../types/LoginType";

export const AdminService = {
  async sendLogin(formValues: LoginType) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/admin/connect`,
        formValues
      );
      render(<Toaster success toastMsg="Connexion réussie !" />);
      return res.data;
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
  async findAllStudents() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/students`);
      return res.data.students;
    } catch (error) {
      console.log(error);
      render(
        <Toaster
          error
          toastMsg="La récupération de la liste des élèves a échouée !"
        />
      );
    }
  },
  async getAdminDetails(id: number) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/admin/getAdminDetails`,
        { id }
      );
      return res.data.admin;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des infos a échouée !" />
      );
    }
  },
  async getStudentInfos(email: string, year: number) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/admin/student/infos/${email}`,
        { year }
      );
      console.log(
        "🚀 ~ file: admin-api.tsx ~ line 63 ~ getStudentInfos ~ res.data.studentInfos",
        res.data.studentInfos
      );
      return res.data.studentInfos;
    } catch (error) {
      console.log(error);
      render(
        <Toaster
          error
          toastMsg="La récupération de la liste des élèves a échouée !"
        />
      );
    }
  },
  async getAllClasses() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/allSchoolClass`);
      return res;
    } catch (error) {
      console.log(error);
      render(
        <Toaster
          error
          toastMsg="La récupérations de la liste des classes a échouée !"
        />
      );
    }
  },
  async createLesson(lesson: CreateLessonType) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/admin/createLesson`,
        lesson
      );
      render(<Toaster success toastMsg="La création du cours a réussie !" />);
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La création du cours a échouée !" />);
    }
  },
  async updateLesson(lesson: UpdateLessonType) {
    try {
      await axios.post(`${configEnv.API_URL}/admin/updateLesson`, lesson);
      render(
        <Toaster success toastMsg="La modification du cours a réussie !" />
      );
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La modification du cours a échouée !" />);
    }
  },

  async getAllSubjects() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/subjects`);
      return res.data;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des sujets a échouée !" />
      );
    }
  },

  async getAllTeachers() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/teachers`);
      return res.data;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des professeurs a échouée !" />
      );
    }
  },
  async getAllClassrooms() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/classrooms`);
      return res.data;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des classrooms a échouée !" />
      );
    }
  },
  async getAllLessons() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/lessons`);
      return res.data;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des lessons a échouée !" />
      );
    }
  },
  async createNote(notePayload: CreateNoteType) {
    try {
      await axios.post(
        `${configEnv.API_URL}/admin/createStudentScore`,
        notePayload
      );
      render(<Toaster success toastMsg="La création de la note a réussie !" />);
    } catch (error) {
      console.log(error);
      render(<Toaster error toastMsg="La création de la note a échouée !" />);
    }
  },

  async updateNote(notePayload: any) {
    try {
      await axios.post(
        `${configEnv.API_URL}/admin/updateStudentScore`,
        notePayload
      );
      render(
        <Toaster success toastMsg="La modification de la note a réussie !" />
      );
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La modification de la note a échouée !" />
      );
    }
  },
  async getAllEvaluations() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/evaluations`);
      return res.data.evaluations;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des évaluations a échouée !" />
      );
    }
  },
  async getAllScores() {
    try {
      const res = await axios.get(`${configEnv.API_URL}/admin/scores`);
      return res.data.scores;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération des scores a échouée !" />
      );
    }
  },
  async getSchoolClassSchedule(body: any) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/admin/getSchoolClassSchedule`,
        body
      );
      return res.data.schedule;
    } catch (error) {
      console.log(error);
      render(
        <Toaster error toastMsg="La récupération du calendrier a échouée !" />
      );
    }
  },
};
