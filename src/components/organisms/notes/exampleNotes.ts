export const exampleNotes = [
  {
    module: "4TECH1",
    subject: "4FULLFT",
    score: 18.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'John Doe',
  },
  {
    module: "4TECH1",
    subject: "4FULLBK",
    score: 19.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Jeanne Doe',
  },
  {
    module: "4DATASOFT1",
    subject: "4DATAVIZ",
    score: 15.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Dummy Doe',
  },
  {
    module: "4SPEDAD1",
    subject: "4FULLF2",
    score: 13.5,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Agility Master Doe',
  },
  {
    module: "4SPEDAD1",
    subject: "4FULLB2",
    score: 20,
    startTime: new Date(2022, 1, 2, 4, 30),
    intervenant: 'Poulet Yassaaaa',
  },
];
