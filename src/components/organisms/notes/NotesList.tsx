import { useState } from "react";
import { EnumTextSize } from "../../../types/enumsSizes";
import BubbleText from "../../atoms/BubbleText/BubbleText";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import BubbleBox from "../../molecules/BubbleBox/BubbleBox";
import WrapperCenter from "../wrapper/WrapperCenter";
import { useMediaQuery } from "react-responsive";

type Note = {
  module: string;
  subject: string;
  score: number;
  startTime: Date;
  intervenant: string;
};

type NotesProps = {
  notes?: Note[];
};

const NotesList = ({ notes }: NotesProps) => {
  const [tech, setTech] = useState<boolean>(false);
  const [datasoft, setDatasoft] = useState<boolean>(false);
  const [spe, setSpe] = useState<boolean>(false);

  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 570px)" });
  const responsiveBubbleBoxWidth = !isTabletOrMobile ? "350px" : "305px";

  const toggleTech = () => {
    setTech(!tech);
  };
  const toggleDatasoft = () => {
    setDatasoft(!datasoft);
  };
  const toggleSpe = () => {
    setSpe(!spe);
  };

  const filterNotesTech: Note[] | undefined = notes?.filter(
    (val) => val.module === "4TECH1"
  );
  const filterNotesDatasoft: Note[] | undefined = notes?.filter(
    (val) => val.module === "4DATASOFT1"
  );
  const filterNotesSpe: Note[] | undefined = notes?.filter(
    (val) => val.module === "4SPEDAD1"
  );
  return (
    <WrapperCenter aos="fade-right">
      <Box width="70%" minHeight="450px">
        <Text value="Modules" size={EnumTextSize.fsXxl} weight="bold" margin="0 0 20px 0" />
        <BubbleText
          value={"4TECH1"}
          margin="20px 0 0 0"
          cursor="pointer"
          handleClick={toggleTech}
        />
        {tech &&
          filterNotesTech?.map((note, index) => {
            return (
              <BubbleBox
                width={responsiveBubbleBoxWidth}
                value={note.subject}
                note={note.score}
                key={index}
              />
            );
          })}
        <BubbleText
          value={"4DATASOFT1"}
          margin="20px 0 0 0"
          cursor="pointer"
          handleClick={toggleDatasoft}
        />
        {datasoft &&
          filterNotesDatasoft?.map((note, index) => {
            return (
              <BubbleBox
                width={responsiveBubbleBoxWidth}
                value={note.subject}
                note={note.score}
                key={index}
              />
            );
          })}
        <BubbleText
          value={"4SPEDAD"}
          margin="20px 0 0 0"
          cursor="pointer"
          handleClick={toggleSpe}
        />
        {spe &&
          filterNotesSpe?.map((note, index) => {
            return (
              <BubbleBox
                width={responsiveBubbleBoxWidth}
                value={note.subject}
                note={note.score}
                key={index}
              />
            );
          })}
      </Box>
    </WrapperCenter>
  );
};

export default NotesList;
