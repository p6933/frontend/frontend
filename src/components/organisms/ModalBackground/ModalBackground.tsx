import styles from "./modal-background.module.scss";

type Props = {
  children: any;
  minHeight?: any;
  height?: any;
};

const ModalBackground = ({
  children,
  minHeight,
  height = "70%",
}: Props) => {
  return (
    <div className={styles.modalBackground} style={{ minHeight, height }}>
      {children}
    </div>
  );
};

export default ModalBackground;
