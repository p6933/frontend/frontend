import { logoPath } from "../../../svgPaths/logo-paths";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import AnimatedSvg from "../../atoms/AnimatedSvg/AnimatedSvg";
import Logo from "../../atoms/Logo/Logo";
import Text from "../../atoms/Text/Text";
import styles from "./page-header.module.scss";
import { useMediaQuery } from "react-responsive";

type Props = {
  value: string;
  animated?: boolean;
  path?: any;
};

const PageHeader = ({ value, animated = false, path = logoPath }: Props) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 500px)" });
  const responsiveTextSize = !isTabletOrMobile
  ? EnumTextSize.fsBigger
  : EnumTextSize.fsMax;
  return (
    <div data-aos="fade-right" className={styles.pageHeader}>
      <Text
        value={value}
        size={responsiveTextSize}
        weight={"bold"}
        fontStyle="italic"
      />
      {!animated ? (
        <Logo margin="0 0 40px 0" resize="250px" />
      ) : (
        <AnimatedSvg
          path={path}
          stroke={EnumColors.primary}
          strokeWidth="4"
          width={"300px"}
          height="300px"
          scale={3}
          top="50px"
          left={"170px"}
        />
      )}
    </div>
  );
};

export default PageHeader;
