import { useNavigate } from "react-router-dom";
import Icon from "../../atoms/Icon/Icon";
import Link from "../../atoms/Link/Link";
import Navlink from "../../molecules/Navlink/Navlink";
import Navlogo from "../../molecules/Navlogo/Navlogo";
import styles from "./navbar.module.scss";
import { AiFillHome as HomeIcon, AiTwotoneSetting } from "react-icons/ai";
import { BsFillInfoCircleFill as AboutIcon } from "react-icons/bs";
import { BsFillCalendarDayFill as CalendarIcon } from "react-icons/bs";
import { FiLogIn as LoginIcon } from "react-icons/fi";
import ProfileAvatar from "../../atoms/ProfileAvatar/ProfileAvatar";
import IconProvider from "../../providers/IconProvider";
import NavDropdown from "../NavDropdown/NavDropdown";
import { useContext, useState } from "react";
import { BiMenuAltRight as NavIcon } from "react-icons/bi";
import MobileNavbar from "../MobileNavbar/MobileNavbar";
import { AuthContext } from "../../providers/AuthContextProvider";

const Navbar = () => {
  const [modal, setModal] = useState(false);
  const [mobileNav, setMobileNav] = useState(false);

  const authContext = useContext(AuthContext);

  let navigate = useNavigate();

  const navigateHome = () => {
    if (authContext.state.userType === "ADMIN") {
      navigate("/admin/dashboard");
    } else if (authContext.state.userType === "STUDENT") {
      navigate("/student/home");
    } else {
      navigate("/");
    }
  };
  const toggleModal = () => setModal(!modal);

  return (
    <IconProvider color="#fff">
      <div data-aos="fade-down" className={styles.navbar}>
        <Navlogo handleClick={navigateHome} />
        <div className={styles.rightblock}>
          {/* Student */}
          {authContext.state.isLoggedIn &&
            authContext.state.userType === "STUDENT" && (
              <>
                <Navlink>
                  <Icon icon={<HomeIcon />} />
                  <Link text={"Accueil"} path={"/student/home"} />
                </Navlink>
                <Navlink>
                  <Icon icon={<AboutIcon />} />
                  <Link text={"Tableau de bord"} path={"/student/dashboard"} />
                </Navlink>
                <Navlink>
                  <Icon icon={<AboutIcon />} />
                  <Link text={"A propos"} path={"/about"} />
                </Navlink>
                <Navlink>
                  <Icon icon={<CalendarIcon />} />
                  <Link text={"Calendrier"} path={"/calendar"} />
                </Navlink>
              </>
            )}
          {/* Admin */}

          {authContext.state.isLoggedIn &&
            authContext.state.userType === "ADMIN" && (
              <>
                <Navlink>
                  <Icon icon={<AiTwotoneSetting />} />
                  <Link text={"Accueil"} path={"/admin/dashboard"} />
                </Navlink>
                <Navlink>
                  <Icon icon={<AboutIcon />} />
                  <Link text={"A propos"} path={"/about"} />
                </Navlink>
                <Navlink>
                  <Icon icon={<CalendarIcon />} />
                  <Link text={"Calendrier"} path={"/calendar"} />
                </Navlink>
              </>
            )}
          {!authContext.state.isLoggedIn && (
            <>
              <Navlink>
                <Icon icon={<AboutIcon />} />
                <Link text={"A propos"} path={"/about"} />
              </Navlink>
              <Navlink>
                <Icon icon={<LoginIcon />} />
                <Link text={"Connexion"} path={"/"} />
              </Navlink>
            </>
          )}
          {authContext.state.isLoggedIn && (
            <ProfileAvatar
              src={authContext.state.currentUser?.profilePicture}
              handleClick={toggleModal}
              background={"#f3f5f7"}
              border={"1px solid #fff"}
            />
          )}
        </div>
        <Icon
          className={styles.navIcon}
          icon={<NavIcon />}
          resize="40px"
          cursor="pointer"
          handleClick={() => {
            setMobileNav(true);
          }}
        />
      </div>
      <NavDropdown setModal={setModal} modal={modal} />
      {mobileNav && <MobileNavbar setModal={setMobileNav} />}
    </IconProvider>
  );
};

export default Navbar;
