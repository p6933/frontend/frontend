import { EnumTextSize } from "../../../types/enumsSizes";
import Logo from "../../atoms/Logo/Logo";
import Text from "../../atoms/Text/Text";
import styles from "./hello-user.module.scss";
import Box from "../../molecules/Box/Box";
import { format } from "../../../utils/format";
import { useContext } from "react";
import { AuthContext } from "../../providers/AuthContextProvider";

const HelloUser = () => {
   const context = useContext(AuthContext)
   const currentUser = context.state.currentUser
  const dummyData = {
    fname: currentUser?.firstName ? currentUser?.firstName : 'John',
    lname: currentUser?.lastName ? currentUser?.lastName : 'Doe',
    date: format(new Date()),
    class: "E4 DAD A",
  };
  return (
    <div data-aos="fade-right" className={styles.helloUser}>
      <Text
        value="Accueil"
        size={EnumTextSize.fsMax}
        weight={"bold"}
        fontStyle="italic"
      />
      <Logo margin="0 0 40px 0" />
      <Box>
        <Text value={"Bienvenue"} size={EnumTextSize.fsBig} />
        <Text
          value={`${dummyData.fname} ${dummyData.lname}`}
          size={EnumTextSize.fsBig}
          margin="0 0 10px 0"
        />
        <Text value={dummyData.date} />
        <Text value={dummyData.class} />
      </Box>
    </div>
  );
};

export default HelloUser;
