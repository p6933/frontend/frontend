import { EnumTextSize } from "../../../types/enumsSizes";
import { addEvent } from "../../../utils/event-utils";
import { actionOnKey } from "../../../utils/keydowns";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import FormContainer from "../FormContainer/FormContainer";
import ModalBackground from "../ModalBackground/ModalBackground";
import Column from "../wrapper/Column";
import ModalWrapper from "../wrapper/ModalWrapper";
import { useMediaQuery } from "react-responsive";
import InputSelectOption from "../../atoms/InputSelectOption/InputSelectOption";
import Spacer from "../../atoms/Spacer/Spacer";
import LabelInput from "../../molecules/LabelInput/LabelInput";
import { useCreateLessonForm } from "../CreateLessonForm/hooks/useCreateLessonForm";

type Props = {
  handleClose: any;
  setEvents: any;
  events: any;
};

const ModalCreateEvent = ({ handleClose, events, setEvents }: Props) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 750px)" });

  const responsiveTextSize = !isTabletOrMobile
    ? EnumTextSize.fsXl
    : EnumTextSize.fsBig;

  actionOnKey("Escape", handleClose);

  const {
    classesList,
    subjectsList,
    teachersList,
    classroomsList,
    createLesson,
  } = useCreateLessonForm();

  const onSubmit = (formValues: any) => {
    const lesson = {
      startDate: formValues.startDate,
      endDate: formValues.endDate,
      schoolClass: parseInt(formValues.schoolClass),
      subject: parseInt(formValues.subject),
      teaching: parseInt(formValues.teaching),
      classRoom: parseInt(formValues.classRoom),
    };
    createLesson(lesson);
    handleClose();
  };

  const extractedTeachers = teachersList?.map((teacher) => {
    return {
      id: teacher.id,
      name: `${teacher.user?.firstName} ${teacher.user?.lastName}`,
    };
  }) ?? [{ id: null, name: null }];

  return (
    <div>
      <ModalWrapper handleClose={handleClose}>
        <ModalBackground height={"90%"}>
          <Box height="100%" width="80%" padding=" 40px 80px">
            <Text
              value="Création d'un évènement"
              weight="bold"
              size={responsiveTextSize}
            />
            <FormContainer
              onSubmit={onSubmit}
              style={{
                width: "100%",
                height: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Column width="30%" margin="20px 0">
                <Text value="Date de début" size={EnumTextSize.fsMedium} />
                <Input name="startDate" type="datetime-local" />
              </Column>
              <Column width="30%" margin="10px 0 60px 0">
                <Text value="Date de fin" size={EnumTextSize.fsMedium} />
                <Input name="endDate" type="datetime-local" />
              </Column>
              {classesList && (
                <LabelInput label={<Text value="Classe" fontStyle="italic" />}>
                  <InputSelectOption
                    name={"schoolClass"}
                    data={classesList}
                    datatype="object-v1-list"
                  />
                </LabelInput>
              )}
              {subjectsList && (
                <LabelInput label={<Text value="Subject" fontStyle="italic" />}>
                  <InputSelectOption
                    name={"subject"}
                    data={subjectsList}
                    datatype="object-v1-list"
                  />
                </LabelInput>
              )}
              {teachersList && (
                <LabelInput label={<Text value="Teacher" fontStyle="italic" />}>
                  <InputSelectOption
                    name={"teaching"}
                    data={extractedTeachers}
                    datatype="object-v1-list"
                  />
                </LabelInput>
              )}
              {classroomsList && (
                <LabelInput
                  label={<Text value="Classroom" fontStyle="italic" />}
                >
                  <InputSelectOption
                    name={"classRoom"}
                    data={classroomsList}
                    datatype="object-v1-list"
                  />
                </LabelInput>
              )}
              <Spacer height={20} />
              <Button text="Créer le cours" width="200px" type="submit" />
            </FormContainer>
          </Box>
        </ModalBackground>
      </ModalWrapper>
    </div>
  );
};

export default ModalCreateEvent;
