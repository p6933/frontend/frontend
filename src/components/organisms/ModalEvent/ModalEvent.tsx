import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { Schedule } from "../../../types/student/schedule/ScheduleDto";
import { formatWithHour } from "../../../utils/format";
import { actionOnKey } from "../../../utils/keydowns";
import BubbleText from "../../atoms/BubbleText/BubbleText";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import ModalBackground from "../ModalBackground/ModalBackground";
import Grid3 from "../wrapper/Grid3";
import ModalWrapper from "../wrapper/ModalWrapper";

type Props = {
  event: Schedule;
  handleClose: () => void;
};
const ModalEvent = ({ event, handleClose }: Props) => {
  const {
    classRoomDesc,
    classRoomName,
    lessonEndDate,
    lessonStartDate,
    subjectDescription,
    subjectName,
    teacherFirstName,
    teacherLastName,
  } = event;

  actionOnKey("Escape", handleClose);

  return (
    <ModalWrapper handleClose={handleClose}>
      <ModalBackground>
        <Box
          width="80%"
          justifyContent="flex-start"
          padding="40px"
          minWidth="90%"
          minHeight="80%"
        >
          <Text
            value={subjectName}
            size={EnumTextSize.fsXl}
            margin="0 0 20px 0"
            weight="bold"
          />
          <Grid3>
            <div>
              <BubbleText value={"Heure de début"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={formatWithHour(lessonStartDate)}
              />
            </div>
            <div>
              <BubbleText value={"Heure de fin"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={formatWithHour(lessonEndDate)}
              />
            </div>
            <div>
              <BubbleText value={"Description"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={subjectDescription}
                whiteSpace="normal"
              />
            </div>
            <div>
              <BubbleText value={"Lieu"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={classRoomName}
              />
            </div>
            <div>
              <BubbleText value={"Description de la salle"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={classRoomDesc}
              />
            </div>
            <div>
              <BubbleText value={"Intervenant"} />
              <BubbleText
                background={EnumColors.light}
                color={EnumColors.dark}
                value={`${teacherFirstName} ${teacherLastName}`}
              />
            </div>
          </Grid3>
        </Box>
      </ModalBackground>
    </ModalWrapper>
  );
};

export default ModalEvent;
