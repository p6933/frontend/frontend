import { useContext, useState } from "react";
import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import LoginHeader from "../../molecules/LoginHeader/LoginHeader";
import FormContainer from "../FormContainer/FormContainer";
import ProfLogin from "../Login/ProfLogin";
import StudentLogin from "../Login/StudentLogin";
import styles from "./signin.module.scss";
import { LoginType } from "../../../types/LoginType";
import { StudentService } from "../../../api/student-api";
import { TeacherService } from "../../../api/teacher-api";
import ModalConfirmation from "../ModalConfirmation/ModalConfirmation";
import { AuthContext } from "../../providers/AuthContextProvider";
import useToken from "../../../hooks/useToken";
import { useNavigate } from "react-router-dom";

const SignIn = () => {
  const [accountType, setAccountType] = useState<"student" | "prof">("student");
  const [modal, setModal] = useState<boolean>(false);
  const [values, setValues] = useState<any>(null);

  const authContext = useContext(AuthContext);
  const navigate = useNavigate();

  const { tokenProfileInfos, tokenId, token } = useToken();

  const closeModal = () => setModal(false);
  const openConfirmationModal = (formValues: LoginType) => {
    setValues(formValues);
    setModal(true);
  };

  const studentMode = () => setAccountType("student");
  const profMode = () => setAccountType("prof");

  const studentLogin = async () => {
    try {
      const res = await StudentService.sendLogin(values);
      authContext.state.login(res.token);
      authContext.dispatch({
        type: "LOGIN",
        payload: {
          isLoggedIn: true,
          currentUser: tokenProfileInfos(),
          userId: tokenId(),
          userType: "STUDENT",
        },
      });
      navigate("/student/home");
    } catch (error) {
      console.error(error);
    }
  };

  const submitSelected = async () => {
    accountType === "student" && studentLogin();
    accountType === "prof" && (await TeacherService.sendLogin(values));
    setModal(false);
  };

  return (
    <>
      <FormContainer onSubmit={openConfirmationModal} className={styles.form}>
        <Text
          value="Connexion"
          size={EnumTextSize.fsMax}
          margin="10px"
          fontStyle="italic"
          weight="bold"
        />
        <Box width="70%" height="400px" border="4px solid #9166FF">
          <LoginHeader
            studentClick={studentMode}
            profClick={profMode}
            accountType={accountType}
          />
          {accountType === "prof" && <ProfLogin />}
          {accountType === "student" && <StudentLogin />}
        </Box>
      </FormContainer>
      {modal && (
        <ModalConfirmation
          handleClose={closeModal}
          method={submitSelected}
          text="Voulez-vous vous connecter ?"
        />
      )}
    </>
  );
};

export default SignIn;
