import { useState } from "react";
import { StudentInfosDto } from "../../../types/student/infos/StudentInfosDto";
import { format } from "../../../utils/format";
import Button from "../../atoms/Button/Button";
import Box from "../../molecules/Box/Box";
import Infos from "../../molecules/Infos/Infos";
import Flex from "../wrapper/Flex";

type Props = {
  studentInfos: StudentInfosDto;
};

const ProfileInfos = ({ studentInfos }: Props) => {
  const {
    address,
    birthdate,
    email,
    firstName,
    lastName,
    phone,
    profilePicture,
  } = studentInfos;
  const [modify, setModify] = useState<boolean>(false);

  const toggleModify = () => {
    setModify(!modify);
  };
  const sendReq = (formValues: any) => console.log(formValues);

  return (
    <Box
      aos="fade-right"
      width="100%"
      justifyContent="flex-start"
      padding="10px"
      margin="50px 0 0 0"
    >
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name={"profilePicture"}
          label="Avatar"
          value={profilePicture}
          type={"file"}
          isSrc
        />
      </Flex>
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name={"lastName"}
          label="Nom"
          value={lastName}
        />
      </Flex>
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name={"firstName"}
          label="Prénom"
          value={firstName}
        />
      </Flex>
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name="email"
          label="Email"
          value={email}
        />
      </Flex>
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name={"birthdate"}
          label={"Date de naissance"}
          value={format(birthdate)}
        />
      </Flex>
      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name="address"
          label="Adresse"
          value={address}
        />
      </Flex>

      <Flex>
        <Infos
          modify={modify}
          hasError={false}
          onSubmit={sendReq}
          name="phone"
          label="Téléphone"
          value={`${phone}`}
        />
      </Flex>
      <Button text="Modifier" handleClick={toggleModify} />
    </Box>
  );
};

export default ProfileInfos;
