import { Dispatch, SetStateAction } from "react";
import { AdminService } from "../../../api/admin-api";
import { StudentsListPayload } from "../../../types/admin/StudentListPayload";
import { EnumTextSize } from "../../../types/enumsSizes";
import { paginate } from "../../../utils/pagination";
import ProfileAvatar from "../../atoms/ProfileAvatar/ProfileAvatar";
import Text from "../../atoms/Text/Text";
import PaginationHandler from "../../molecules/PaginationHandler/PaginationHandler";
import FlexCenter from "../wrapper/FlexCenter";
import styles from "./user-list.module.scss";

type Props = {
  users: StudentsListPayload[];
  setStudentInfos: Dispatch<SetStateAction<any>>;
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  filterLastName?: string | null | undefined;
  filterFirstName?: string | null | undefined;
  filterYear: number;
  filterClass?: string | null | undefined;
  filterSpe?: string | null | undefined;
  openModal: () => void;
};

const UserList = ({
  users,
  filterLastName,
  filterFirstName,
  filterClass,
  filterSpe,
  filterYear,
  page,
  setPage,
  setStudentInfos,
  openModal,
}: Props) => {
  // filter user with search options
  const filteredUsers = users
    ?.filter((user) => {
      if (filterLastName)
        return user?.lastName
          ?.toLowerCase()
          .includes(filterLastName.toLowerCase());
      return user;
    })
    .filter((user) => {
      if (filterFirstName)
        return user.firstName
          ?.toLowerCase()
          .includes(filterFirstName.toLowerCase());
      return user;
    })
    .filter((user) => {
      if (filterClass) {
        if (user.schoolClassNames) {
          const res = user.schoolClassNames?.filter((schoolClassName: any) => {
            return schoolClassName
              ?.toLowerCase()
              .includes(filterClass.toLowerCase());
          });
          if (res.length > 0) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return user;
      }
    });
  // .filter((user) => {
  //   if (filterYear) {
  //     const inscriptionYear = user.inscriptions?.filter(
  //       (inscription: any) => {
  //         const year = inscription.startDate?.toString().slice(0, 4);
  //         return year.includes(filterYear);
  //       }
  //     );
  //     return inscriptionYear;
  //   }
  //   return user;
  // });
  // .filter((user) => {
  //   if (filterSpe) return user.speciality?.includes(filterSpe);
  //   return user;
  // });

  const itemsNumberOnPage = 5;

  const paginatedUsers: StudentsListPayload[][] = paginate(
    filteredUsers,
    itemsNumberOnPage
  );

  const nextPage = () => {
    if (0 <= page && page <= filteredUsers.length / itemsNumberOnPage - 1) {
      setPage(page + 1);
    }
  };

  const prevPage = () => {
    if (0 < page && page <= filteredUsers.length / itemsNumberOnPage) {
      setPage(page - 1);
    }
  };

  const fetchStudentInfos = async (email: string, year = 2021) => {
    const studentsInfos = await AdminService.getStudentInfos(email, year);
    setStudentInfos(studentsInfos);
    openModal();
  };

  return (
    <div className={styles.userList} data-aos="zoom-in">
      {paginatedUsers[page]?.map((user: any, index: number) => {
        return (
          <FlexCenter padding="10px" key={index}>
            <ProfileAvatar border="none" src={user?.profilePicture} />
            <Text
              value={`${user?.firstName} ${user?.lastName}`}
              cursor="pointer"
              padding="5px"
              size={EnumTextSize.fsMedium}
              handleClick={() => fetchStudentInfos(user.email, filterYear)}
            />
          </FlexCenter>
        );
      })}
      <PaginationHandler page={page} next={nextPage} prev={prevPage} />
    </div>
  );
};

export default UserList;
