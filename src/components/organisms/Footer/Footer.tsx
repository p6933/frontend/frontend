import { AiOutlineInstagram, AiOutlineLinkedin } from "react-icons/ai";
import { ImTwitter as Twitter } from "react-icons/im";
import { TiWorldOutline } from "react-icons/ti";
import { EnumColors } from "../../../types/enumsColors";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import IconProvider from "../../providers/IconProvider";
import FlexCenter from "../wrapper/FlexCenter";
import styles from "./footer.module.scss";

type Props = {};

const Footer = (props: Props) => {
  return (
    <div className={styles.footer}>
      <FlexCenter width="100%">
        <Text
          value={"Estiam 2020 © Tous droits réservés"}
          color={EnumColors.light}
          fontStyle="italic"
        />
        <IconProvider color={EnumColors.light}>
          <Icon
            icon={<TiWorldOutline />}
            margin={"5px 0 0 15px"}
            cursor="pointer"
          />
          <Icon
            icon={<AiOutlineInstagram />}
            margin={"5px 0 0 15px"}
            cursor="pointer"
          />
          <Icon
            icon={<AiOutlineLinkedin />}
            margin={"5px 0 0 15px"}
            cursor="pointer"
          />
          <Icon icon={<Twitter />} margin={"5px 0 0 15px"} cursor="pointer" />
        </IconProvider>
      </FlexCenter>
    </div>
  );
};

export default Footer;
