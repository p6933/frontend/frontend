import { useMemo, useState } from "react";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { Ticket } from "../../../types/TicketType";
import BubbleText from "../../atoms/BubbleText/BubbleText";
import Button from "../../atoms/Button/Button";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import ModalTicket from "../ModalTicket/ModalTicket";
import WrapperCenter from "../wrapper/WrapperCenter";

type Props = {
  tickets: Ticket[];
};

const Tickets = ({ tickets }: Props) => {
  const [toggle, setToggle] = useState<boolean>(false);

  //close modal ticket creation
  const handleClose = () => setToggle(false);
  const toggleModal = () => setToggle(true);

  const filterOpenTicket = useMemo(
    () => tickets.filter((ticket) => ticket.status === "OPEN"),
    [tickets]
  );

  const filterClosedTicket = useMemo(
    () => tickets.filter((ticket) => ticket.status === "CLOSED"),
    [tickets]
  );

  return (
    <>
      <WrapperCenter aos="fade-right">
        <Box width="70%" minHeight="450px">
          <Text value="Tickets" size={EnumTextSize.fsXxl} margin="0 0 20px 0" />
          <BubbleText
            value={"Tickets en cours"}
            margin="20px 0 0 0"
            cursor="pointer"
          />
          {filterOpenTicket.map((ticket, index) => {
            return (
              <BubbleText
                value={ticket.subject}
                background={EnumColors.light}
                color={EnumColors.dark}
                key={index}
              />
            );
          })}
          <BubbleText
            value={"Tickets terminés"}
            margin="20px 0 0 0"
            cursor="pointer"
          />
          {filterClosedTicket.map((ticket, index) => {
            return (
              <BubbleText
                value={ticket.subject}
                background={EnumColors.light}
                color={EnumColors.dark}
                key={index}
              />
            );
          })}
          <Button
            text="Créer un ticket"
            margin="20px 0 0 0"
            width="200px"
            background={EnumColors.light}
            color={EnumColors.primary}
            handleClick={toggleModal}
          />
        </Box>
      </WrapperCenter>
      {toggle && <ModalTicket handleClose={handleClose} />}
    </>
  );
};

export default Tickets;
