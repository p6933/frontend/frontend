import styles from "./scores-list.module.scss";
import Text from "../../atoms/Text/Text";
import { EnumTextSize } from "../../../types/enumsSizes";
import { useHandleNotes } from "./hooks/useHandleNotes";
import ScoreCard from "../ScoreCard/ScoreCard";
import LoadingSpinner from "../../atoms/LoadingSpinner/LoadingSpinner";

type Props = {};

const ScoresList = (props: Props) => {
  const { scores, isScoresLoading } = useHandleNotes();
  const sortedScores = scores?.sort((a, b) => {
    if (
      a.student?.user?.lastName &&
      b.student?.user?.lastName &&
      a.student?.user?.lastName < b.student?.user?.lastName
    ) {
      return -1;
    }
    if (
      a.student?.user?.lastName &&
      b.student?.user?.lastName &&
      a.student?.user?.lastName > b.student?.user?.lastName
    ) {
      return 1;
    }
    return 0;
  });
  return (
    <div className={styles.scoreList}>
      {isScoresLoading ? (
        <LoadingSpinner />
      ) : (
        <>
          {!isScoresLoading && sortedScores ? (
            <>
              <div className={styles.headerScores}>
                <Text
                  value={"SUBJECT"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />

                <Text
                  value={"START DATE"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"END DATE"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"TEACHER"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />

                <Text
                  value={"STUDENT"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"SCORE"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
              </div>
              {sortedScores.map((score) => (
                <>
                  <ScoreCard score={score} />
                </>
              ))}
            </>
          ) : (
            <div>No scores</div>
          )}
        </>
      )}
    </div>
  );
};

export default ScoresList;
