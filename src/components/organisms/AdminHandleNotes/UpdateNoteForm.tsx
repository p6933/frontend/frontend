import Button from "../../atoms/Button/Button";
import InputSelectOption from "../../atoms/InputSelectOption/InputSelectOption";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import LabelInput from "../../molecules/LabelInput/LabelInput";
import FormContainer from "../FormContainer/FormContainer";
import { useHandleNotes } from "./hooks/useHandleNotes";

type Props = {};

const UpdateNoteForm = (props: Props) => {
  const scoresModel = [
    "0",
    "2",
    "4",
    "1",
    "3",
    "5",
    "6",
    "9",
    "7",
    "8",
    "10",
    "11",
    "12",
    "13",
    "15",
    "16",
    "14",
    "17",
    "18",
    "19",
    "20",
  ];

  const {
    students,
    evaluations,
    updateNote,
    isEvaluationsLoading,
    isStudentsLoading,
  } = useHandleNotes();

  const onSubmit = (formValues: any) => {
    console.log(
      "🚀 ~ file: UpdateNoteForm.tsx ~ line 45 ~ onSubmit ~ formValues",
      formValues
    );
    let isAbsent: boolean;
    isAbsent = formValues.isAbsent === "present" && false;
    isAbsent = formValues.isAbsent === "absent" && true;
    const notePayload = {
      isAbsent: isAbsent,
      score: parseInt(formValues.score),
      studentId: parseInt(formValues.studentId),
      id: parseInt(formValues.id),
    };
    console.log(
      "🚀 ~ file: UpdateNoteForm.tsx ~ line 54 ~ onSubmit ~ notePayload",
      notePayload
    );
    updateNote(notePayload);
  };

  const extractedStudents = students?.map((student) => {
    return {
      id: student.id,
      name: `${student.firstName} ${student.lastName}`,
    };
  }) ?? [{ id: null, name: null }];

  const extractedEvaluations = evaluations?.map((evaluation) => {
    return {
      id: evaluation.id,
      name: evaluation.subject?.name,
    };
  }) ?? [{ id: null, name: null }];

  return (
    <FormContainer
      onSubmit={onSubmit}
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      {!isStudentsLoading && students && (
        <LabelInput label={<Text value="Student" fontStyle="italic" />}>
          <InputSelectOption
            name={"studentId"}
            data={extractedStudents}
            datatype="object-v1-list"
          />
        </LabelInput>
      )}
      <LabelInput label={<Text value="Presence" fontStyle="italic" />}>
        <InputSelectOption name={"isAbsent"} data={["absent", "present"]} />
      </LabelInput>
      <LabelInput label={<Text value="Score" fontStyle="italic" />}>
        <InputSelectOption name={"score"} data={scoresModel} />
      </LabelInput>

      {!isEvaluationsLoading && evaluations && (
        <LabelInput label={<Text value="Evaluation" fontStyle="italic" />}>
          <InputSelectOption
            name={"id"}
            data={extractedEvaluations}
            datatype="object-v1-list"
          />
        </LabelInput>
      )}
      <Spacer height={20} />
      <Button text="Modifier la note" width="200px" type="submit" />
    </FormContainer>
  );
};

export default UpdateNoteForm;
