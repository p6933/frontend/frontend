import { useCallback, useEffect, useState } from "react";
import { AdminService } from "../../../../api/admin-api";
import { CreateNoteType } from "../../../../types/admin/CreateNoteType";
import { Evaluation } from "../../../../types/evaluations/EvaluationType";
import { Score } from "../../../../types/scores/ScoreType";

export const useHandleNotes = () => {
  const [students, setStudents] = useState<any[] | null>(null);
  const [evaluations, setEvaluations] = useState<Evaluation[] | null>(null);
  const [scores, setScores] = useState<Score[] | null>(null);
  const [isStudentsLoading, setIsStudentsLoading] = useState<boolean>(false);
  const [isScoresLoading, setIsScoresLoading] = useState<boolean>(false);
  const [isEvaluationsLoading, setIsEvaluationsLoading] =
    useState<boolean>(false);

  const createNote = useCallback(async (notePayload: CreateNoteType) => {
    try {
      await AdminService.createNote(notePayload);
    } catch (error) {
      console.log(
        "🚀 ~ file: useHandleNotes.tsx ~ line 10 ~ createNote ~ error",
        error
      );
    }
  }, []);

  const updateNote = useCallback(async (notePayload: any) => {
    try {
      await AdminService.updateNote(notePayload);
    } catch (error) {
      console.log(
        "🚀 ~ file: useHandleNotes.tsx ~ line 10 ~ createNote ~ error",
        error
      );
    }
  }, []);

  const fetchStudents = useCallback(async () => {
    try {
      setIsStudentsLoading(true);
      const dataStudents = await AdminService.findAllStudents();

      setStudents(dataStudents);
      setIsStudentsLoading(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: useHandleNotes.tsx ~ line 34 ~ fetchStudents ~ error",
        error
      );
      setIsStudentsLoading(false);
    }
  }, []);

  const fetchScores = useCallback(async () => {
    try {
      setIsScoresLoading(true);
      const dataScores = await AdminService.getAllScores();

      setScores(dataScores);
      setIsScoresLoading(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: useHandleNotes.tsx ~ line 34 ~ fetchScores ~ error",
        error
      );
      setIsScoresLoading(false);
    }
  }, []);

  const fetchEvaluations = useCallback(async () => {
    try {
      setIsEvaluationsLoading(true);
      const dataEvaluations = await AdminService.getAllEvaluations();

      setEvaluations(dataEvaluations);
      setIsEvaluationsLoading(false);
    } catch (error) {
      console.log(
        "🚀 ~ file: useHandleNotes.tsx ~ line 58 ~ fetchEvaluations ~ error",
        error
      );

      setIsEvaluationsLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchStudents();
    fetchEvaluations();
    fetchScores();
  }, [fetchStudents, fetchEvaluations, fetchScores]);

  return {
    createNote,
    isStudentsLoading,
    students,
    evaluations,
    isEvaluationsLoading,
    scores,
    isScoresLoading,
    updateNote,
  };
};
