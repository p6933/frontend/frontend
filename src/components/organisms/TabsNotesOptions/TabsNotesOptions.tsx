import { useState } from "react";
import { EnumTabNotesOptions } from "../../../types/EnumTabNotesOption";
import styles from "./tabs-notes-options.module.scss";

type Props = {
  changeTab: (option: EnumTabNotesOptions) => void;
  tab: EnumTabNotesOptions;
};

const TabsNotesOptions = ({ changeTab, tab }: Props) => {
  return (
    <div className={styles.headerNotes}>
      <div className={styles.tabs}>
        {tab === EnumTabNotesOptions.list && (
          <>
            <p onClick={() => changeTab(EnumTabNotesOptions.create)}>CREATE</p>
            <p
              className={styles.activeTab}
              onClick={() => changeTab(EnumTabNotesOptions.list)}
            >
              LIST
            </p>
            <p onClick={() => changeTab(EnumTabNotesOptions.update)}>UPDATE</p>
          </>
        )}
        {tab === EnumTabNotesOptions.create && (
          <>
            <p
              onClick={() => changeTab(EnumTabNotesOptions.create)}
              className={styles.activeTab}
            >
              CREATE
            </p>
            <p onClick={() => changeTab(EnumTabNotesOptions.list)}>LIST</p>
            <p onClick={() => changeTab(EnumTabNotesOptions.update)}>UPDATE</p>
          </>
        )}
        {tab === EnumTabNotesOptions.update && (
          <>
            <p onClick={() => changeTab(EnumTabNotesOptions.create)}>CREATE</p>
            <p onClick={() => changeTab(EnumTabNotesOptions.list)}>LIST</p>
            <p
              onClick={() => changeTab(EnumTabNotesOptions.update)}
              className={styles.activeTab}
            >
              UPDATE
            </p>
          </>
        )}
      </div>
    </div>
  );
};

export default TabsNotesOptions;
