import styles from "./wrapper.module.scss";

type Props = {
  children?: any;
  aos?: string;
};

const Wrapper = ({ aos, children }: Props) => {
  return (
    <div data-aos={aos} className={styles.wrapper}>
      {children}
    </div>
  );
};

export default Wrapper;
