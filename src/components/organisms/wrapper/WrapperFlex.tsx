import styles from "./wrapper.module.scss";

type Props = {
  children: any;
};

const WrapperFlex = ({ children }: Props) => {
  return <div className={styles.wrapperFlex}>{children}</div>;
};

export default WrapperFlex;
