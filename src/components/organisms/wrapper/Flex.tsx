import styles from "./wrapper.module.scss";

type Props = {
  children?: any;
  width?: string;
  margin?: string;
  padding?: string;
  height?: string;
  flexDirection?: "row" | "row-reverse" | "column" | "column-reverse";
};

const Flex = ({ children, width, margin, padding, height, flexDirection }: Props) => {
  return (
    <div
      className={styles.flex}
      style={{ width, margin, padding, height, flexDirection }}
    >
      {children}
    </div>
  );
};

export default Flex;
