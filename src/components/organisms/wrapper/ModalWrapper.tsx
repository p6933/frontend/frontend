import Icon from "../../atoms/Icon/Icon";
import styles from "./wrapper.module.scss";
import { MdClose } from "react-icons/md";
import IconProvider from "../../providers/IconProvider";
import { EnumColors } from "../../../types/enumsColors";

type Props = { children: any; handleClose: any; zIndex?: string };

const ModalWrapper = ({ children, handleClose, zIndex }: Props) => {
  return (
    <div className={styles.modalWrapper} style={{ zIndex }}>
      <IconProvider color={EnumColors.light}>
        <Icon
          icon={<MdClose />}
          position="absolute"
          right="5px"
          top="70px"
          resize="60px"
          cursor="pointer"
          handleClick={handleClose}
        />
      </IconProvider>
      {children}
    </div>
  );
};

export default ModalWrapper;
