import styles from "./wrapper.module.scss";

type Props = {
  children: any;
  aos?: any;
};

const WrapperCenter = ({ children, aos }: Props) => {
  return (
    <div data-aos={aos} className={styles.wrapperCenter}>
      {children}
    </div>
  );
};

export default WrapperCenter;
