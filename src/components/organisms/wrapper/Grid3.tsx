import styles from "./wrapper.module.scss";

type Props = {
  children?: any;
};

const Grid3 = (props: Props) => {
  return <div className={styles.grid3}>{props.children}</div>;
};

export default Grid3;
