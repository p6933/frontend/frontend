import styles from "./wrapper.module.scss";

type Props = {
    children: any
}

const WrapperSplit = ({children}: Props) => {
  return (
    <div className={styles.wrapperSplit}>
        {children}
    </div>
  )
}

export default WrapperSplit