import styles from "./wrapper.module.scss";

type Props = {
  children: any;
  width?: string;
  margin?: string;
  padding?: string;
  height?: string;
  aos?: string;
};

const FlexCenter = ({
  children,
  width,
  margin,
  padding,
  height,
  aos,
}: Props) => {
  return (
    <div
      data-aos={aos}
      className={styles.flexCenter}
      style={{ width, margin, padding, height }}
    >
      {children}
    </div>
  );
};

export default FlexCenter;
