import styles from "./wrapper.module.scss";

type Props = {
  children?: any;
  width?: string;
  margin?: string;
  padding?: string;
  height?: string;
};

const Column = ({ children, width, margin, padding, height }: Props) => {
  return (
    <div className={styles.column} style={{ width, margin, padding, height }}>
      {children}
    </div>
  );
};

export default Column;
