import { StudentNotesPayload } from "../../../types/student/infos2/StudentNotesPayload";
import { StudentPersonalInfos } from "../../../types/student/infos2/StudentPersonalInfos";
import { StudentPresenceInfosPayload } from "../../../types/student/infos2/StudentPresenceInfosPayload";
import NotesInfos from "../NotesInfos/NotesInfos";
import PersonalInfos from "../PersonalInfos/PersonalInfos";
import PresenceInfos from "../PresenceInfos/PresenceInfos";
import ModalWrapper from "../wrapper/ModalWrapper";
import styles from "./modal-student-infos-admin.module.scss";

type Props = {
  handleClose: () => void;
  studentPersonalInfos: StudentPersonalInfos;
  presence: StudentPresenceInfosPayload;
  studentNotes: StudentNotesPayload;
};

const ModalStudentInfosAdmin = ({
  handleClose,
  presence,
  studentNotes,
  studentPersonalInfos,
}: Props) => {
  return (
    <>
      <ModalWrapper handleClose={handleClose} zIndex="30">
        <div className={styles.gridInfos}>
          <PersonalInfos studentPersonalInfos={studentPersonalInfos} />
          <PresenceInfos presence={presence} />
          <NotesInfos studentNotes={studentNotes} />
        </div>
      </ModalWrapper>
    </>
  );
};

export default ModalStudentInfosAdmin;
