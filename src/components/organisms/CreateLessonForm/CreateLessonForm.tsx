import { EnumTextSize } from "../../../types/enumsSizes";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import InputSelectOption from "../../atoms/InputSelectOption/InputSelectOption";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import LabelInput from "../../molecules/LabelInput/LabelInput";
import FormContainer from "../FormContainer/FormContainer";
import Column from "../wrapper/Column";
import { useCreateLessonForm } from "./hooks/useCreateLessonForm";

type Props = {};

const CreateLessonForm = (props: Props) => {
  const {
    classesList,
    subjectsList,
    teachersList,
    classroomsList,
    createLesson,
  } = useCreateLessonForm();

  const onSubmit = (formValues: any) => {
    const lesson = {
      startDate: formValues.startDate,
      endDate: formValues.endDate,
      schoolClass: parseInt(formValues.schoolClass),
      subject: parseInt(formValues.subject),
      teaching: parseInt(formValues.teaching),
      classRoom: parseInt(formValues.classRoom),
    };
    createLesson(lesson);
  };

  const extractedTeachers = teachersList?.map((teacher) => {
    return {
      id: teacher.id,
      name: `${teacher.user?.firstName} ${teacher.user?.lastName}`,
    };
  }) ?? [{ id: null, name: null }];

  return (
    <div>
      <FormContainer
        onSubmit={onSubmit}
        style={{
          width: "100%",
          height: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Column width="30%" margin="20px 0">
          <Text value="Date de début" size={EnumTextSize.fsMedium} />
          <Input name="startDate" type="datetime-local" />
        </Column>
        <Column width="30%" margin="10px 0 60px 0">
          <Text value="Date de fin" size={EnumTextSize.fsMedium} />
          <Input name="endDate" type="datetime-local" />
        </Column>
        {classesList && (
          <LabelInput label={<Text value="Classe" fontStyle="italic" />}>
            <InputSelectOption
              name={"schoolClass"}
              data={classesList}
              datatype="object-v1-list"
            />
          </LabelInput>
        )}
        {subjectsList && (
          <LabelInput label={<Text value="Subject" fontStyle="italic" />}>
            <InputSelectOption
              name={"subject"}
              data={subjectsList}
              datatype="object-v1-list"
            />
          </LabelInput>
        )}
        {teachersList && (
          <LabelInput label={<Text value="Teacher" fontStyle="italic" />}>
            <InputSelectOption
              name={"teaching"}
              data={extractedTeachers}
              datatype="object-v1-list"
            />
          </LabelInput>
        )}
        {classroomsList && (
          <LabelInput label={<Text value="Classroom" fontStyle="italic" />}>
            <InputSelectOption
              name={"classRoom"}
              data={classroomsList}
              datatype="object-v1-list"
            />
          </LabelInput>
        )}
        <Spacer height={20} />
        <Button text="Créer le cours" width="200px" type="submit" />
      </FormContainer>
    </div>
  );
};

export default CreateLessonForm;
