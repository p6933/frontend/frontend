import { useCallback, useEffect, useState } from "react";
import { AdminService } from "../../../../api/admin-api";
import { CreateLessonType } from "../../../../types/admin/CreateLessonType";
import { Lesson } from "../../../../types/lesson/LessonType";
import { Subject } from "../../../../types/subject/SubjectType";
import { Teacher } from "../../../../types/teacher/TeacherType";

export const useCreateLessonForm = () => {
  const [classesList, setClassesList] = useState<
    { name: string; id: number }[] | null
  >(null);

  const [subjects, setSubjects] = useState<Subject[] | null>(null);
  const [teachers, setTeachers] = useState<Teacher[] | null>(null);
  const [classrooms, setClassrooms] = useState<any[] | null>(null);
  const [lessonCreated, setLessonCreated] = useState<boolean>(false);
  const [lessons, setLessons] = useState<Lesson[] | null>(null);

  const [isLessonsLoading, setIsLessonsLoading] = useState<boolean>(false);

  const createLesson = async (lesson: CreateLessonType) => {
    try {
      await AdminService.createLesson(lesson);
      setLessonCreated(!lessonCreated);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 24 ~ createLesson ~ error",
        error
      );
    }
  };

  const updateLesson = async (lesson: any) => {
    try {
      await AdminService.updateLesson(lesson);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 37 ~ updateLesson ~ error",
        error
      );
    }
  };

  const fetchLessons = useCallback(async () => {
    try {
      setIsLessonsLoading(true);
      const res = await AdminService.getAllLessons();
      const dataLessons = res?.lessons;
      setLessons(dataLessons);
      setIsLessonsLoading(false);
    } catch (error) {
      setIsLessonsLoading(false);
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 43 ~ error",
        error
      );
    }
  }, []);

  const fetchClasses = useCallback(async () => {
    try {
      const res = await AdminService.getAllClasses();
      const schoolClasses = res?.data?.schoolClasses;
      const listOfClasses = schoolClasses.map((schoolClass: any) => {
        return { name: schoolClass.name, id: schoolClass.id };
      });
      setClassesList(listOfClasses);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 66 ~ fetchClasses ~ error",
        error
      );
    }
  }, []);

  const fetchSubjects = useCallback(async () => {
    try {
      const res: any = await AdminService.getAllSubjects();
      const dataSubjects = res?.subjects;
      setSubjects(dataSubjects);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 76 ~ fetchSubjects ~ error",
        error
      );
    }
  }, []);

  const fetchTeachers = useCallback(async () => {
    try {
      const res: any = await AdminService.getAllTeachers();
      const dataTeachers = res?.teachers;
      setTeachers(dataTeachers);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 89 ~ error",
        error
      );
    }
  }, []);

  const fetchClassrooms = useCallback(async () => {
    try {
      const res: any = await AdminService.getAllClassrooms();

      const dataClassrooms = res?.classrooms;
      setClassrooms(dataClassrooms);
    } catch (error) {
      console.log(
        "🚀 ~ file: useCreateLessonForm.tsx ~ line 101 ~ fetchClassrooms ~ error",
        error
      );
    }
  }, []);

  useEffect(() => {
    fetchClasses();
    fetchSubjects();
    fetchTeachers();
    fetchClassrooms();
    fetchLessons();
  }, [
    fetchClasses,
    fetchSubjects,
    fetchTeachers,
    fetchClassrooms,
    fetchLessons,
  ]);

  return {
    fetchClasses,
    classesList,
    fetchSubjects,
    fetchTeachers,
    subjectsList: subjects,
    teachersList: teachers,
    classroomsList: classrooms,
    createLesson,
    lessons,
    fetchLessons,
    isLessonsLoading,
    updateLesson,
  };
};
