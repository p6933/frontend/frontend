import styles from "./lesson-list.module.scss";
import LessonCard from "../LessonCard/LessonCard";
import { useCreateLessonForm } from "./hooks/useCreateLessonForm";
import Text from "../../atoms/Text/Text";
import { EnumTextSize } from "../../../types/enumsSizes";
import LoadingSpinner from "../../atoms/LoadingSpinner/LoadingSpinner";

type Props = {};

const LessonList = (props: Props) => {
  const { lessons, isLessonsLoading } = useCreateLessonForm();

  const sortedLessons = lessons?.sort((a, b) => {
    if (
      a.subject?.name &&
      b.subject?.name &&
      a.subject?.name < b.subject?.name
    ) {
      return -1;
    }
    if (
      a.subject?.name &&
      b.subject?.name &&
      a.subject?.name > b.subject?.name
    ) {
      return 1;
    }
    return 0;
  });
  return (
    <div className={styles.lessonList}>
      {isLessonsLoading ? (
        <LoadingSpinner />
      ) : (
        <>
          {!isLessonsLoading && sortedLessons ? (
            <>
              <div className={styles.headerLessons}>
                <Text
                  value={"SUBJECT"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"START DATE"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"END DATE"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
                <Text
                  value={"CLASSROOM"}
                  size={EnumTextSize.fsMedium}
                  weight="bold"
                />
              </div>
              {sortedLessons.map((lesson) => (
                <>
                  <LessonCard lesson={lesson} key={lesson.id} />
                </>
              ))}
            </>
          ) : (
            <div>No lessons</div>
          )}
        </>
      )}
    </div>
  );
};

export default LessonList;
