import { EnumTextSize } from "../../../types/enumsSizes";
import { actionOnKey } from "../../../utils/keydowns";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import FormContainer from "../FormContainer/FormContainer";
import ModalBackground from "../ModalBackground/ModalBackground";
import Column from "../wrapper/Column";
import ModalWrapper from "../wrapper/ModalWrapper";

type Props = {
  handleClose: any;
};

const ModalTicket = ({ handleClose }: Props) => {
  const onSubmit = (formValues: { subject: string; pjointe?: any }) => {
    console.log(formValues);
  };

  actionOnKey("Escape", handleClose);

  return (
    <ModalWrapper handleClose={handleClose}>
      <ModalBackground>
        <Box
          height="80%"
          width="80%"
          justifyContent="flex-start"
          padding="40px"
        >
            <Text value="Création de ticket" size={EnumTextSize.fsXl} margin='0 0 20px 0' weight="bold" />

          <FormContainer
            onSubmit={onSubmit}
            style={{
              width: "100%",
              height: "100%",
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center'
            }}
          >
            <Column>
              <Text value="Sujet" size={EnumTextSize.fsMedium} />
              <Input name="subject" height="150px" width="500px" type="field" />
            </Column>
            <Column margin="50px 0">
              <Text value="Pièce jointe" size={EnumTextSize.fsMedium} />
              <Input name="pjointe" type="file" />
            </Column>
            <Button text="Créer" width="200px" />
          </FormContainer>
        </Box>
      </ModalBackground>
    </ModalWrapper>
  );
};

export default ModalTicket;
