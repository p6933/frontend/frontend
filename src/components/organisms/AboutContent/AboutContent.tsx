import { EnumTextSize } from "../../../types/enumsSizes";
import Line from "../../atoms/Line/Line";
import Text from "../../atoms/Text/Text";
import styles from "./about-content.module.scss";
import { useMediaQuery } from "react-responsive";

const AboutContent = () => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 500px)" });
  const responsiveTextWidth = !isTabletOrMobile ? "400px" : "300px";
  const responsiveTextSize = !isTabletOrMobile
    ? EnumTextSize.fsMax
    : EnumTextSize.fsXxl;
  return (
    <div data-aos="fade-right" className={styles.aboutContent}>
      <Text
        value="Tout tes besoins regroupés en 1 !"
        fontStyle="italic"
        size={responsiveTextSize}
        weight="bold"
        whiteSpace="normal"
      />
      <Line margin="40px" />
      <Text
        value={`Cette application te permet d'avoir acces de manière simple, autonome et efficace à l'ensemble des fonctionnalités de l'école`}
        size={EnumTextSize.fsBig}
        width={responsiveTextWidth}
        whiteSpace="normal"
      />
    </div>
  );
};

export default AboutContent;
