import {
  login1Path,
  login2Path,
  login3Path,
  login4Path,
  login5Path,
  login6Path,
  login7Path,
  login8Path,
} from "../../../svgPaths/login-paths";
import { EnumColors } from "../../../types/enumsColors";
import AnimatedSvg from "../../atoms/AnimatedSvg/AnimatedSvg";
import RandomImage from "../../atoms/RandomImage/RandomImage";
import WrapperCenter from "../wrapper/WrapperCenter";
import { useMediaQuery } from "react-responsive";


const LoginIllustration = () => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 500px)" });
  const responsiveSvgSize = !isTabletOrMobile ? "100%" : "90%";

  <AnimatedSvg
    path={login3Path}
    stroke={EnumColors.primary}
    width={responsiveSvgSize}
    height={responsiveSvgSize}
    left="40px"
  />;
  const arraySvg = [
    <AnimatedSvg
      path={login1Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login2Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login3Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login4Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login5Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login6Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login7Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
    <AnimatedSvg
      path={login8Path}
      stroke={EnumColors.primary}
      width={responsiveSvgSize}
      height={responsiveSvgSize}
      left="40px"
    />,
  ];
  return (
    <>
      <WrapperCenter>
        <RandomImage images={arraySvg} type="component" />
      </WrapperCenter>
    </>
  );
};

export default LoginIllustration;
