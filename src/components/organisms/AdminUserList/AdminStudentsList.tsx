import { Dispatch, SetStateAction, useState } from "react";
import { EnumTextSize } from "../../../types/enumsSizes";
import Button from "../../atoms/Button/Button";
import InputSelectOption from "../../atoms/InputSelectOption/InputSelectOption";
import Line from "../../atoms/Line/Line";
import LoadingSpinner from "../../atoms/LoadingSpinner/LoadingSpinner";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import LabelInput from "../../molecules/LabelInput/LabelInput";
import SearchBar from "../../molecules/SearchBar/SearchBar";
import FormContainer from "../FormContainer/FormContainer";
import UserList from "../UserList/UserList";

import styles from "./admin-user-list.module.scss";
type Props = {
  users: any[];
  setStudentInfos: Dispatch<SetStateAction<number>>;
  openModal: () => void;
};

const AdminStudentsList = ({ users, setStudentInfos, openModal }: Props) => {
  const [filterFirstName, setFilterFirstName] = useState<
    string | null | undefined
  >(null);
  const [filterLastName, setFilterLastName] = useState<
    string | null | undefined
  >(null);
  const [filterClass, setFilterClass] = useState<string | null | undefined>(
    null
  );
  const [filterYear, setFilterYear] = useState<number>(2021);
  const [filterSpe, setFilterSpe] = useState<string | null | undefined>(null);
  const [page, setPage] = useState(0);
  return (
    <>
      <Box height="100%" width="100%" margin="10px">
        <Text value={"Etudiants"} size={EnumTextSize.fsBig} margin={"10px"} />
        <FormContainer
          onSubmit={(formValue: any) => {
            console.log(formValue);
            setFilterLastName(formValue.lname);
            setFilterFirstName(formValue.fname);
            setFilterClass(formValue.class);
            setFilterSpe(formValue.speciality);
            setFilterYear(formValue.year);
            setPage(0);
          }}
          aos="zoom-in"
        >
          <SearchBar value={"Prénom de l'étudiant"} name={"fname"} />
          <SearchBar value={"Nom de famille de l'étudiant"} name={"lname"} />
          <div className={styles.flex}>
            <LabelInput label={<Text value="Année" fontStyle="italic" />}>
              <InputSelectOption
                name={"year"}
                data={[2022, 2021, 2020, 2019, 2000]}
              />
            </LabelInput>
            <LabelInput label={<Text value="Classe" fontStyle="italic" />}>
              <InputSelectOption
                name={"class"}
                data={["21-22 E4 DAD A", "E4 DAD B", "E4 CCSN A", "E4 MBA A"]}
              />
            </LabelInput>
            <LabelInput label={<Text value="Spécialité" fontStyle="italic" />}>
              <InputSelectOption
                name={"speciality"}
                data={["Finance", "Retail"]}
              />
            </LabelInput>
          </div>
          <Button margin="10px" text="Rechercher" type="submit" />
        </FormContainer>
        <Line />
        {users ? (
          <UserList
            openModal={openModal}
            setStudentInfos={setStudentInfos}
            page={page}
            setPage={setPage}
            users={users}
            filterFirstName={filterFirstName}
            filterLastName={filterLastName}
            filterClass={filterClass}
            filterSpe={filterSpe}
            filterYear={filterYear}
          />
        ) : (
          <LoadingSpinner />
        )}
      </Box>
    </>
  );
};

export default AdminStudentsList;
