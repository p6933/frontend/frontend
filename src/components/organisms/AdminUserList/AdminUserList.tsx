import { useEffect, useState } from "react";
import { AdminService } from "../../../api/admin-api";
import LoadingSpinner from "../../atoms/LoadingSpinner/LoadingSpinner";
import ModalStudentInfosAdmin from "../ModalStudentInfosAdmin/ModalStudentInfosAdmin";
import AdminStudentsList from "./AdminStudentsList";
import AdminTeachersList from "./AdminTeachersList";

type Props = {
  mode: "teachers" | "students";
};

const AdminUserList = ({ mode }: Props) => {
  const [users, setUsers] = useState<any>();
  const [studentInfos, setStudentInfos] = useState<any>();
  const [modal, setModal] = useState<boolean>(false);

  const fetchStudents = async () => {
    const students = await AdminService.findAllStudents();
    setUsers(students);
  };

  const closeModal = () => setModal(false);
  const openModal = () => setModal(true);

  useEffect(() => {
    fetchStudents();
  }, []);

  return (
    <>
      {mode === "students" && users ? (
        <AdminStudentsList
          openModal={openModal}
          setStudentInfos={setStudentInfos}
          users={users}
        />
      ) : (
        <LoadingSpinner />
      )}
      {mode === "teachers" && <AdminTeachersList />}
      {modal && studentInfos && (
        <ModalStudentInfosAdmin
          handleClose={closeModal}
          studentPersonalInfos={studentInfos.studentPersonalInfos}
          presence={studentInfos.presence}
          studentNotes={studentInfos.studentNotes}
        />
      )}
    </>
  );
};

export default AdminUserList;
