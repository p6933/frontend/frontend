import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import SearchBar from "../../molecules/SearchBar/SearchBar";
import FormContainer from "../FormContainer/FormContainer";

type Props = {};

const AdminTeachersList = (props: Props) => {
  return (
    <>
      <Box height="100%" width="100%" margin="10px">
        <Text value={"Teachers"} size={EnumTextSize.fsBig} margin={"10px"} />
        <FormContainer
          onSubmit={(formValue: any) => {
            console.log(formValue);
          }}
        >
          <SearchBar value={"Recherche"} name={"student"} />
        </FormContainer>
      </Box>
    </>
  );
};

export default AdminTeachersList;
