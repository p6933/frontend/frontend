import styles from "./personal-infos.module.scss";
import { BiEdit } from "react-icons/bi";
import { EnumTextSize } from "../../../types/enumsSizes";
import { StudentPersonalInfos } from "../../../types/student/infos2/StudentPersonalInfos";
import { format } from "../../../utils/format";
import Icon from "../../atoms/Icon/Icon";
import ProfileAvatar from "../../atoms/ProfileAvatar/ProfileAvatar";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import WhiteCard from "../../atoms/WhiteCard/WhiteCard";
import TextUnderline from "../../molecules/TextUnderline/TextUnderline";
import Flex from "../wrapper/Flex";
import FlexCenter from "../wrapper/FlexCenter";
import IconProvider from "../../providers/IconProvider";
import { EnumColors } from "../../../types/enumsColors";
import { useState } from "react";
import ModalEdit from "../ModalEdit/ModalEdit";
import { EnumStudentInfosOptions } from "../../../types/EnumStudentInfosOptions";

type Props = {
  studentPersonalInfos: StudentPersonalInfos;
};

const PersonalInfos = ({ studentPersonalInfos }: Props) => {
  const [modalEdit, setModalEdit] = useState<boolean>(false);
  const [currentInfo, setCurrentInfo] = useState<any>();
  const [editName, setEditName] = useState<EnumStudentInfosOptions>(
    EnumStudentInfosOptions.avatar
  );
  const { birthDate, email, firstName, lastName, phone, profilePicture } =
    studentPersonalInfos;

  const openEditModal = (option: EnumStudentInfosOptions) => {
    switch (option) {
      case EnumStudentInfosOptions.avatar:
        setEditName(EnumStudentInfosOptions.avatar);
        setCurrentInfo(profilePicture);
        break;
      case EnumStudentInfosOptions.firstName:
        setEditName(EnumStudentInfosOptions.firstName);
        setCurrentInfo(firstName);
        break;
      case EnumStudentInfosOptions.lastName:
        setEditName(EnumStudentInfosOptions.lastName);
        setCurrentInfo(lastName);
        break;
      case EnumStudentInfosOptions.email:
        setEditName(EnumStudentInfosOptions.email);
        setCurrentInfo(email);
        break;
      case EnumStudentInfosOptions.birthdate:
        setEditName(EnumStudentInfosOptions.birthdate);
        setCurrentInfo(birthDate);
        break;
      case EnumStudentInfosOptions.phone:
        setEditName(EnumStudentInfosOptions.phone);
        setCurrentInfo(phone);
        break;

      default:
        break;
    }
    setModalEdit(true);
  };
  const closeEditModal = () => {
    setModalEdit(false);
  };
  return (
    <WhiteCard>
      <IconProvider color={EnumColors.primary}>
        <FlexCenter height="80px">
          <Text
            value={"Infos étudiant"}
            weight="bold"
            fontStyle="italic"
            size={EnumTextSize.fsBig}
          />
        </FlexCenter>
        <Flex padding="10px 0">
          <TextUnderline text={"Avatar"} />
          <ProfileAvatar
            src={profilePicture}
            border="none"
            handleClick={() => openEditModal(EnumStudentInfosOptions.avatar)}
          />
        </Flex>
        <Flex padding="10px 0">
          <TextUnderline text={"Nom"} />
          <div className={styles.blocInfo}>
            <Icon
              icon={<BiEdit />}
              handleClick={() =>
                openEditModal(EnumStudentInfosOptions.lastName)
              }
              cursor="pointer"
            />
            <Spacer width={8} />
            {lastName ? <Text value={lastName} /> : "none"}
          </div>
        </Flex>
        <Flex padding="10px 0">
          <TextUnderline text={"Prénom"} />
          <div className={styles.blocInfo}>
            <Icon
              icon={<BiEdit />}
              handleClick={() =>
                openEditModal(EnumStudentInfosOptions.firstName)
              }
              cursor="pointer"
            />
            <Spacer width={8} />
            {firstName ? <Text value={firstName} /> : "none"}
          </div>
        </Flex>
        <Flex padding="10px 0">
          <TextUnderline text={"Email"} />
          <div className={styles.blocInfo}>
            <Icon
              icon={<BiEdit />}
              handleClick={() => openEditModal(EnumStudentInfosOptions.email)}
              cursor="pointer"
            />
            <Spacer width={8} />
            {email ? <Text value={email} /> : "none"}
          </div>
        </Flex>
        <Flex padding="10px 0">
          <TextUnderline text={"Date de naissance"} />
          <div className={styles.blocInfo}>
            <Icon
              icon={<BiEdit />}
              handleClick={() =>
                openEditModal(EnumStudentInfosOptions.birthdate)
              }
              cursor="pointer"
            />
            <Spacer width={8} />
            {birthDate ? <Text value={format(birthDate)} /> : "none"}
          </div>
        </Flex>
        <Flex padding="10px 0">
          <TextUnderline text={"Téléphone"} />
          <div className={styles.blocInfo}>
            <Icon
              icon={<BiEdit />}
              handleClick={() => openEditModal(EnumStudentInfosOptions.phone)}
              cursor="pointer"
            />
            <Spacer width={8} />
            {phone ? <Text value={phone} /> : "none"}
          </div>
        </Flex>
      </IconProvider>
      {modalEdit && (
        <ModalEdit
          handleClose={closeEditModal}
          name={editName}
          currentInfo={currentInfo}
        />
      )}
    </WhiteCard>
  );
};

export default PersonalInfos;
