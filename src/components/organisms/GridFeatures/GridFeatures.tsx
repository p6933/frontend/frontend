import styles from './grid-features.module.scss'

type Props = {
    children: any
}

const GridFeatures = ({children}: Props) => {
  return (
    <div className={styles.gridFeatures}>{children}</div>
  )
}

export default GridFeatures