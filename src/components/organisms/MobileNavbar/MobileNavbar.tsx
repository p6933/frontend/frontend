import { Dispatch, SetStateAction, useContext, useState } from "react";
import Icon from "../../atoms/Icon/Icon";
import styles from "./mobile-navbar.module.scss";
import { IoReturnUpBack as ArrowBack } from "react-icons/io5";
import ProfileAvatar from "../../atoms/ProfileAvatar/ProfileAvatar";
import { actionOnKey } from "../../../utils/keydowns";
import Text from "../../atoms/Text/Text";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { useNavigate } from "react-router-dom";
import AnimatedSvg from "../../atoms/AnimatedSvg/AnimatedSvg";
import { logoPath } from "../../../svgPaths/logo-paths";
import { AuthContext } from "../../providers/AuthContextProvider";
import ModalConfirmation from "../ModalConfirmation/ModalConfirmation";

type Props = {
  setModal: Dispatch<SetStateAction<boolean>>;
};

const MobileNavbar = ({ setModal }: Props) => {
  const [confirmationModal, setConfirmationModal] = useState<boolean>(false);

  const openConfirmationModal = () => {
    setConfirmationModal(true);
  };
  const closeConfirmationModal = () => setConfirmationModal(false);

  const authContext = useContext(AuthContext);

  let navigate = useNavigate();
  actionOnKey("Escape", () => setModal(false));

  const logoutUser = () => {
    authContext.state.logout();
    authContext.dispatch({ type: "LOGOUT" });
    closeConfirmationModal();
    setModal(false);
    navigate("/");
  };

  const navigateHome = () => {
    if (authContext.state.userType === "ADMIN") {
      navigate("/admin/dashboard");
    } else if (authContext.state.userType === "STUDENT") {
      navigate("/student/home");
    } else {
      navigate("/");
    }
    setModal(false);
  };
  const goPath = (path: string) => {
    setModal(false);
    navigate(path);
  };
  return (
    <>
      <div data-aos="fade-up" className={styles.mobileNavbar}>
        <div className={styles.navlogopath}>
          <AnimatedSvg
            path={logoPath}
            position="absolute"
            left={"-35px"}
            top="-60px"
            scale={0.5}
            stroke={EnumColors.light}
            strokeWidth="4px"
            cursor={"pointer"}
            handleClick={navigateHome}
          />
        </div>

        <Icon
          icon={<ArrowBack />}
          position="absolute"
          right="20px"
          top="30px"
          resize="40px"
          cursor="pointer"
          handleClick={() => setModal(false)}
        />
        <ProfileAvatar
          src={authContext.state.currentUser?.profilePicture}
          background={"#f3f5f7"}
          border={"1px solid #fff"}
          resize="120px"
          margin=" 0 0 40px 0"
          handleClick={() => goPath("/profile")}
        />
        {/* Student */}
        {authContext.state.isLoggedIn &&
          authContext.state.userType === "STUDENT" && (
            <>
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Accueil"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => navigateHome()}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Tableau de bord"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => goPath("/student/dashboard")}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"A propos"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => goPath("/about")}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Calendrier"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => goPath("/calendar")}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Déconnexion"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={openConfirmationModal}
              />
            </>
          )}

        {/* Admin */}
        {authContext.state.isLoggedIn &&
          authContext.state.userType === "ADMIN" && (
            <>
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Accueil"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => navigateHome()}
              />

              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"A propos"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => goPath("/about")}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Calendrier"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={() => goPath("/calendar")}
              />
              <Text
                margin="10px"
                size={EnumTextSize.fsBig}
                value={"Déconnexion"}
                color={EnumColors.light}
                cursor="pointer"
                handleClick={openConfirmationModal}
              />
            </>
          )}

        {/* not logged */}
        {!authContext.state.isLoggedIn && (
          <>
            <Text
              margin="10px"
              size={EnumTextSize.fsBig}
              value={"Accueil"}
              color={EnumColors.light}
              cursor="pointer"
              handleClick={() => navigateHome()}
            />
            <Text
              margin="10px"
              size={EnumTextSize.fsBig}
              value={"A propos"}
              color={EnumColors.light}
              cursor="pointer"
              handleClick={() => goPath("/about")}
            />

            <Text
              margin="10px"
              size={EnumTextSize.fsBig}
              value={"Connexion"}
              color={EnumColors.light}
              cursor="pointer"
              handleClick={() => goPath("/")}
            />
          </>
        )}
      </div>
      {confirmationModal && (
        <ModalConfirmation
          handleClose={closeConfirmationModal}
          method={logoutUser}
          text="Vous allez être déconnecté, confirmez-vous ?"
        />
      )}
    </>
  );
};

export default MobileNavbar;
