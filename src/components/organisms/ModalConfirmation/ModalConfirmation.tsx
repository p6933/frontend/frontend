import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { actionOnKey } from "../../../utils/keydowns";
import Button from "../../atoms/Button/Button";
import Text from "../../atoms/Text/Text";
import ModalBackground from "../ModalBackground/ModalBackground";
import Flex from "../wrapper/Flex";
import ModalWrapper from "../wrapper/ModalWrapper";

type Props = {
  handleClose: () => void;
  method: any;
  text?: string;
  zIndex?: string;
};

const ModalConfirmation = ({
  handleClose,
  method,
  text = "Voulez vous continuer votre action ?",
  zIndex,
}: Props) => {
  const confirm = () => {
    handleClose();
    return method();
  };

  actionOnKey("Escape", handleClose);

  return (
    <ModalWrapper handleClose={handleClose} zIndex={zIndex}>
      <ModalBackground height={"300px"}>
        <Text
          value={text}
          size={EnumTextSize.fsBig}
          margin="40px"
          width="100%"
          whiteSpace="normal"
        />
        <Flex width="300px">
          <Button
            text={"Annuler"}
            background={EnumColors.grey}
            color={EnumColors.light}
            border={`1px solid ${EnumColors.grey}`}
            handleClick={handleClose}
          />
          <Button
            text={"Confirmer"}
            background={EnumColors.primary}
            color={EnumColors.light}
            handleClick={confirm}
          />
        </Flex>
      </ModalBackground>
    </ModalWrapper>
  );
};

export default ModalConfirmation;
