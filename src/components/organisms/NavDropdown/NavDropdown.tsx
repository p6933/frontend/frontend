import Icon from "../../atoms/Icon/Icon";
import Navlink from "../../molecules/Navlink/Navlink";
import styles from "./nav-dropdown.module.scss";
import { RiUserSettingsLine as SettingsIcon } from "react-icons/ri";
import { BiPowerOff } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import Text from "../../atoms/Text/Text";
import { EnumTextSize } from "../../../types/enumsSizes";
import { EnumColors } from "../../../types/enumsColors";
import { Dispatch, SetStateAction, useContext, useState } from "react";
import { AuthContext } from "../../providers/AuthContextProvider";
import ModalConfirmation from "../ModalConfirmation/ModalConfirmation";

type Props = {
  modal: boolean;
  setModal: Dispatch<SetStateAction<boolean>>;
};

const NavDropdown = ({ modal, setModal }: Props) => {
  const navigate = useNavigate();
  const authContext = useContext(AuthContext);
  const [confirmationModal, setConfirmationModal] = useState<boolean>(false);

  const closeConfirmationModal = () => setConfirmationModal(false);
  const openConfirmationModal = () => {
    setConfirmationModal(true);
  };

  const logoutUser = () => {
    try {
      authContext.state.logout();
      authContext.dispatch({ type: "LOGOUT" });
      closeConfirmationModal();
      setModal(false);
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {modal && (
        <div className={styles.dropdown} data-aos="fade-down">
          <Navlink
            cursor="pointer"
            handleClick={() => {
              navigate("/profile");
              setModal(false);
            }}
          >
            <Icon icon={<SettingsIcon />} />
            <Text
              margin="0 0 0 4px"
              value="Informations personnelles"
              size={EnumTextSize.fsMedium}
              color={EnumColors.light}
            />
          </Navlink>
          <Navlink
            width="247px"
            cursor="pointer"
            handleClick={openConfirmationModal}
          >
            <Icon icon={<BiPowerOff />} />
            <Text
              margin="0 0 0 4px"
              value="Déconnexion"
              size={EnumTextSize.fsMedium}
              color={EnumColors.light}
            />
          </Navlink>
        </div>
      )}
      {confirmationModal && (
        <ModalConfirmation
          handleClose={closeConfirmationModal}
          method={logoutUser}
          text="Vous allez être déconnecté, confirmez-vous ?"
        />
      )}
    </>
  );
};

export default NavDropdown;
