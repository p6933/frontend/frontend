import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AdminService } from "../../../api/admin-api";
import useToken from "../../../hooks/useToken";
import { EnumTextSize } from "../../../types/enumsSizes";
import { LoginType } from "../../../types/LoginType";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import TextUnderline from "../../molecules/TextUnderline/TextUnderline";
import { AuthContext } from "../../providers/AuthContextProvider";
import FormContainer from "../FormContainer/FormContainer";
import AdminLogin from "../Login/AdminLogin";
import ModalConfirmation from "../ModalConfirmation/ModalConfirmation";

import styles from "./signinAdmin.module.scss";

const SigninAdmin = () => {
  const [modal, setModal] = useState<boolean>(false);
  const [values, setValues] = useState<any>(null);

  const navigate = useNavigate();
  const authContext = useContext(AuthContext);
  const { tokenProfileInfos, tokenId, tokenRole } = useToken();

  const closeModal = () => setModal(false);
  const openConfirmationModal = (formValues: LoginType) => {
    setValues(formValues);
    setModal(true);
  };

  const sendLoginRequest = async () => {
    try {
      const res = await AdminService.sendLogin(values);
      authContext.state.login(res.token);
      authContext.dispatch({
        type: "LOGIN",
        payload: {
          isLoggedIn: true,
          currentUser: tokenProfileInfos(),
          userId: tokenId(),
          userType: tokenRole(),
        },
      });
      navigate("/admin/dashboard");
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      <FormContainer onSubmit={openConfirmationModal} className={styles.form}>
        <Text
          value="Connexion"
          size={EnumTextSize.fsMax}
          margin="10px"
          fontStyle="italic"
          weight="bold"
        />

        <Box width="70%" height="400px" border="4px solid #9166FF">
          <TextUnderline
            text="Admin"
            margin="0 0 20px 30px"
            textWeight="bold"
            lineWidth="30px"
            lineMargin="0 0 0 15px"
          />
          <AdminLogin />
        </Box>
      </FormContainer>
      {modal && (
        <ModalConfirmation
          handleClose={closeModal}
          method={sendLoginRequest}
          text="Voulez-vous vous connecter ?"
        />
      )}
    </>
  );
};

export default SigninAdmin;
