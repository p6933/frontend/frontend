import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import Text from "../../atoms/Text/Text";

type Props = {
    handleClick?: any
};

const StudentLogin = ({handleClick}: Props) => {
  return (
    <>
      <Text value="Email" size={EnumTextSize.fsMedium} />
      <Input name="email" hasError={false} type="email" margin="0 0 20px 0" />
      <Text value="Password" size={EnumTextSize.fsMedium} />
      <Input name="password" hasError={false} type="password" />
      <Text
        color={EnumColors.primary}
        value="Mot de passe oublié ?"
        fontStyle="italic"
        margin="20px 0"
        weight="bold"
        cursor="pointer"
      />
      <Button handleClick={handleClick} text="Envoyer" type="submit" height="40px" margin="10px" />
    </>
  );
};

export default StudentLogin;
