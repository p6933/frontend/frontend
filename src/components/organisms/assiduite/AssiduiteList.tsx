import { Absence } from "../../../types/AbsenceType";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { Retard } from "../../../types/RetardType";
import { format, formatWithHour } from "../../../utils/format";
import BubbleText from "../../atoms/BubbleText/BubbleText";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import WrapperCenter from "../wrapper/WrapperCenter";

type AbsenceProps = {
  absences?: Absence[];
  retards?: Retard[];
};

const AbsenceList = ({ absences, retards }: AbsenceProps) => {
  return (
    <WrapperCenter>
      <Box width="70%" minHeight="450px">
        <Text value="Assiduité" size={EnumTextSize.fsXxl} margin="0 0 20px 0" />
        <BubbleText value={"Absences"} margin="20px 0 0 0" />
        {absences?.map((absence, index) => {
          return (
            <BubbleText
              value={`${absence.subject} - ${format(
                absence.startTime
              )}`}
              key={index}
              background={EnumColors.light}
              color={EnumColors.dark}
            />
          );
        })}
        <BubbleText value={"Retards"} margin="20px 0 0 0" />
        {retards?.map((retard, index) => {
          return (
            <BubbleText
              value={`${retard.subject} - ${formatWithHour(retard.startTime)}`}
              key={index}
              background={EnumColors.light}
              color={EnumColors.dark}
            />
          );
        })}
      </Box>
    </WrapperCenter>
  );
};

export default AbsenceList;
