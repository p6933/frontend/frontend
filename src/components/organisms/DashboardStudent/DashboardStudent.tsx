import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../../atoms/Text/Text";
import Box from "../../molecules/Box/Box";
import TextUnderline from "../../molecules/TextUnderline/TextUnderline";
import styles from "./dashboard-student.module.scss";

type Props = {};

const DashboardStudent = (props: Props) => {
  return (
    <div className={styles.dashboardStudent}>
      <Box width="400px" height="250px">
        <TextUnderline
          text="Remarques"
          textWeight="bold"
          fontSize={EnumTextSize.fsBig}
        />
        <Text
          value="Cours CCNA"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
        <Text
          value="Retard 26/04/2022"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
      </Box>
      <Box width="400px" height="250px">
        <TextUnderline
          text="Absences"
          textWeight="bold"
          fontSize={EnumTextSize.fsBig}
        />
        <Text
          value="15/04/2022"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
        <Text
          value="02/05/2022"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
      </Box>
      <Box width="400px" height="250px">
        <TextUnderline
          text="Retards"
          textWeight="bold"
          fontSize={EnumTextSize.fsBig}
        />
        <Text
          value="26/03/2022"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
        <Text
          value="26/04/2022"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
      </Box>
      <Box width="400px" height="250px">
        <TextUnderline
          text="Tickets"
          textWeight="bold"
          fontSize={EnumTextSize.fsBig}
        />
        <Text
          value="Note VM"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
        <Text
          value="Cours Vendredi"
          weight="bold"
          margin="5px"
          size={EnumTextSize.fsMedium}
        />
      </Box>
    </div>
  );
};

export default DashboardStudent;
