import styles from "./score-card.module.scss";
import { formatWithHour } from "../../../utils/format";
import Text from "../../atoms/Text/Text";
import { Score } from "../../../types/scores/ScoreType";
import { useId } from "react";

type Props = { score: Score };

const ScoreCard = ({ score }: Props) => {
  const id = useId();

  const formatedStartDate =
    score.evaluation?.startDate && formatWithHour(score.evaluation?.startDate);
  const formatedEndDate =
    score.evaluation?.endDate && formatWithHour(score.evaluation?.endDate);

  const teacher = score.evaluation?.subject?.teachings?.[0]?.teacher?.user;

  const student = score.student?.user;
  return (
    <div className={styles.scoreCard} id={id}>
      <Text value={score.evaluation?.subject?.name} />
      <Text value={formatedStartDate} />
      <Text value={formatedEndDate} />
      {teacher ? (
        <Text value={`${teacher?.firstName} ${teacher?.lastName}`} />
      ) : (
        <Text value={"no data"} />
      )}
      <Text value={`${student?.firstName} ${student?.lastName}`} />
      <Text value={`${score.score}`} />
    </div>
  );
};

export default ScoreCard;
