import { Form } from "react-final-form";
import styles from './form-container.module.scss'

type Props = {
  children: any;
  onSubmit?: any;
  style?: any;
  className?: any;
  aos?: any;
};

const FormContainer = ({ children, onSubmit, style, className = styles.formContainer, aos = 'fade-right' }: Props) => {
  return (
    <Form onSubmit={onSubmit}>
      {(props) => (
        <form data-aos={aos} onSubmit={props.handleSubmit} style={style} className={className}>
          {children}
        </form>
      )}
    </Form>
  );
};
export default FormContainer;
