import { EnumTextSize } from "../../../types/enumsSizes";
import { StudentNotesPayload } from "../../../types/student/infos2/StudentNotesPayload";
import Text from "../../atoms/Text/Text";
import WhiteCard from "../../atoms/WhiteCard/WhiteCard";
import BubbleBox from "../../molecules/BubbleBox/BubbleBox";
import FlexCenter from "../wrapper/FlexCenter";
import styles from "./notes-infos.module.scss";

type Props = { studentNotes: StudentNotesPayload };

const NotesInfos = ({ studentNotes }: Props) => {
  return (
    <WhiteCard>
      <FlexCenter height="80px">
        <Text
          value={"Notes"}
          weight="bold"
          fontStyle="italic"
          size={EnumTextSize.fsBig}
        />
      </FlexCenter>
      <div className={styles.notesBox}>
        {studentNotes ? (
          studentNotes.notes?.map((note, index: number) => {
            return (
              <div key={index}>
                {studentNotes.subject && note.score && (
                  <BubbleBox
                    value={studentNotes.subject}
                    note={`${note?.score}`}
                  />
                )}
                {studentNotes.subject && !note.isAbsent && (
                  <BubbleBox value={studentNotes.subject} note={`abs`} />
                )}
              </div>
            );
          })
        ) : (
          <Text value={"Pas de notes"} />
        )}
      </div>
    </WhiteCard>
  );
};

export default NotesInfos;
