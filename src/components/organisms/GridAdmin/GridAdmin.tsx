import styles from "./grid-admin.module.scss";

type Props = {
  children: any;
  right?: string;
  left?: string;
  top?: string;
  bottom?: string;
};

const GridAdmin = ({ children, bottom, left, right, top }: Props) => {
  return (
    <div
      className={styles.gridAdmin}
      style={{
        bottom,
        left,
        right,
        top,
      }}
    >
      {children}
    </div>
  );
};

export default GridAdmin;
