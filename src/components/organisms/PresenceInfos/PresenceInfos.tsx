import { useState } from "react";
import { RiDeleteBack2Fill } from "react-icons/ri";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import { EnumStudentAdminMethods } from "../../../types/enumStudentAdminMethods";
import { StudentPresenceInfo } from "../../../types/student/infos2/StudentPresenceInfo";
import { StudentPresenceInfosPayload } from "../../../types/student/infos2/StudentPresenceInfosPayload";
import { formatWithHour } from "../../../utils/format";
import BubbleText from "../../atoms/BubbleText/BubbleText";
import Icon from "../../atoms/Icon/Icon";
import Line from "../../atoms/Line/Line";
import Spacer from "../../atoms/Spacer/Spacer";
import Text from "../../atoms/Text/Text";
import WhiteCard from "../../atoms/WhiteCard/WhiteCard";
import IconProvider from "../../providers/IconProvider";
import ModalConfirmation from "../ModalConfirmation/ModalConfirmation";
import FlexCenter from "../wrapper/FlexCenter";
import styles from "./presence-infos.module.scss";

type Props = {
  presence: StudentPresenceInfosPayload;
};

const PresenceInfos = ({ presence }: Props) => {
  const absences = presence.filter((p) => p.delay === 0);
  const retards = presence.filter((p) => p.delay && p.delay > 0);

  const handleClick = () => {
    openConfirmationModal();
    setMethod(EnumStudentAdminMethods.deleteAbsence);
  };
  const [modalConfirm, setModalConfirm] = useState<boolean>(false);
  const [method, setMethod] = useState<EnumStudentAdminMethods>(
    EnumStudentAdminMethods.default
  );

  const closeConfirmationModal = () => {
    setModalConfirm(false);
  };
  const openConfirmationModal = () => {
    setModalConfirm(true);
  };

  const handleMethod = (method: EnumStudentAdminMethods) => {
    switch (method) {
      case EnumStudentAdminMethods.deleteAbsence:
        console.log("To be replace with an endpoint who delete absence");
        closeConfirmationModal();
        break;

      default:
        break;
    }
  };
  return (
    <>
      <IconProvider color={EnumColors.light}>
        <WhiteCard>
          <FlexCenter height="80px">
            <Text
              value={"Assiduité"}
              weight="bold"
              fontStyle="italic"
              size={EnumTextSize.fsBig}
            />
          </FlexCenter>
          <Text
            value={"Absences"}
            color={EnumColors.primary}
            width="max-content"
            weight="bold"
            size={EnumTextSize.fsMedium}
          />
          <Spacer height={8} />
          {absences.length > 0 ? (
            absences.map((absence: StudentPresenceInfo) => {
              return (
                <>
                  <Spacer height={8} />
                  <BubbleText>
                    <Text value={absence.moduleName} color={EnumColors.light} />
                    <Spacer width={8} />
                    <Text value="-" color={EnumColors.light} />
                    <Spacer width={8} />
                    {absence.startDate && (
                      <Text
                        value={`${formatWithHour(absence.startDate)}`}
                        color={EnumColors.light}
                      />
                    )}
                    <Spacer width={8} />
                    <Icon
                      icon={<RiDeleteBack2Fill />}
                      cursor="pointer"
                      handleClick={handleClick}
                    />
                  </BubbleText>
                  <Spacer height={8} />
                </>
              );
            })
          ) : (
            <>
              <Spacer height={8} />
              <Text value={"Aucune absence"} color={EnumColors.dark} />
            </>
          )}
          <FlexCenter padding="20px">
            <Line width="100px" height="2px" />
          </FlexCenter>
          <Text
            value={"Retards"}
            color={EnumColors.primary}
            width="max-content"
            weight="bold"
            size={EnumTextSize.fsMedium}
          />
          <Spacer height={8} />
          {retards.length > 0 ? (
            retards.map((retard: StudentPresenceInfo) => {
              return (
                <>
                  <Spacer height={8} />
                  <BubbleText>
                    <Text value={retard.moduleName} color={EnumColors.light} />
                    <Spacer width={8} />
                    <Text value="-" color={EnumColors.light} />
                    <Spacer width={8} />
                    {retard.startDate && (
                      <Text
                        value={`${formatWithHour(retard.startDate)}`}
                        color={EnumColors.light}
                      />
                    )}
                    <Spacer width={8} />
                    <Text value="-" color={EnumColors.light} />
                    <Spacer width={8} />
                    <Text
                      value={`retard: ${retard.delay}min`}
                      color={EnumColors.light}
                    />
                    <Spacer width={8} />
                    <Icon
                      icon={<RiDeleteBack2Fill />}
                      cursor="pointer"
                      handleClick={handleClick}
                    />
                  </BubbleText>
                  <Spacer height={8} />
                </>
              );
            })
          ) : (
            <>
              <Spacer height={8} />
              <Text value={"Aucun retard"} color={EnumColors.dark} />
            </>
          )}
        </WhiteCard>
        {modalConfirm && (
          <ModalConfirmation
            handleClose={closeConfirmationModal}
            method={() => handleMethod(method)}
            text="Voulez-vous vraiment supprimer cet élément ?"
            zIndex={"50px"}
          />
        )}
      </IconProvider>
    </>
  );
};

export default PresenceInfos;
