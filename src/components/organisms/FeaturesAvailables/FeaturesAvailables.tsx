import styles from "./features-availables.module.scss";
import { MyFeature } from "../../../types/myfeature";
import { features } from "../../../utils/testDatas";
import Card from "../../molecules/Card/Card";
import HeadBand from "../../molecules/HeadBand/HeadBand";
import GridFeatures from "../GridFeatures/GridFeatures";

const FeaturesAvailables = () => {
  return (
    <div className={styles.featuresAvailables} data-aos="fade-right">
      <HeadBand />
      <GridFeatures>
        {features?.map((feature: MyFeature) => {
          return <Card feature={feature} />;
        })}
      </GridFeatures>
    </div>
  );
};

export default FeaturesAvailables;
