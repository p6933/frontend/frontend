import { EnumStudentInfosOptions } from "../../../types/EnumStudentInfosOptions";
import Box from "../../molecules/Box/Box";
import SingleInputForm from "../../molecules/SingleInputForm/SingleInputForm";
import TextUnderline from "../../molecules/TextUnderline/TextUnderline";
import ModalBackground from "../ModalBackground/ModalBackground";
import FlexCenter from "../wrapper/FlexCenter";
import ModalWrapper from "../wrapper/ModalWrapper";

type Props = {
  handleClose: () => void;
  name: EnumStudentInfosOptions;
  currentInfo: any;
};

const ModalEdit = ({ handleClose, name, currentInfo }: Props) => {
  return (
    <ModalWrapper handleClose={handleClose}>
      <ModalBackground>
        <Box>
          <TextUnderline text={currentInfo} />
          {name === EnumStudentInfosOptions.avatar ? (
            <SingleInputForm
              name={name}
              type="file"
              onSubmit={(formValues: any) =>
                console.log(
                  "replace by endpoint <updateStudent> from admin",
                  formValues
                )
              }
            />
          ) : (
            <SingleInputForm
              name={name}
              onSubmit={(formValues: any) =>
                console.log(
                  "replace by endpoint <updateStudent> from admin",
                  formValues
                )
              }
            />
          )}
        </Box>
      </ModalBackground>
    </ModalWrapper>
  );
};

export default ModalEdit;
