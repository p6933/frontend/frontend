import styles from "./lesson-card.module.scss";
import { Lesson } from "../../../types/lesson/LessonType";
import { formatWithHour } from "../../../utils/format";
import Text from "../../atoms/Text/Text";
import { useId } from "react";

type Props = { lesson: Lesson };

const LessonCard = ({ lesson }: Props) => {
  const id = useId();

  const formatedStartDate =
    lesson.startDate && formatWithHour(lesson.startDate);
  const formatedEndDate = lesson.endDate && formatWithHour(lesson.endDate);
  return (
    <div className={styles.lessonCard} id={id}>
      <Text value={lesson.subject?.name} />
      <Text value={formatedStartDate} />
      <Text value={formatedEndDate} />
      <Text value={lesson.classRoom?.name} />
    </div>
  );
};

export default LessonCard;
