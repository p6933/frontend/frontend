import { motion } from "framer-motion";
import { pathVariants, svgVariants } from "../../../animations";
import { EnumColors } from "../../../types/enumsColors";
import styles from "./animated-svg.module.scss";

/**
 * exemple de path : M 201.00,75.13
                     C 177.43,77.44 159.48,73.58 148.00,62.60
                     148.00,62.60 148.00,53.76 148.00,53.76
                     148.00,53.76 199.00,53.76 199.00,53.76
                     199.00,53.76 201.00,75.13 201.00,75.13 Z
                     M 196.33,78.13
 */

type Props = {
  className?: string;
  handleClick?: any;
  path: string;
  stroke?: string;
  strokeWidth?: string;
  fill?: string;
  variants?: any;
  width?: any;
  height?: any;
  position?: any;
  top?: any;
  right?: any;
  left?: any;
  bottom?: any;
  scale?: any;
  cursor?: any;
};

const AnimatedSvg = ({
  className = styles.animatedSvg,
  handleClick,
  path,
  stroke = EnumColors.dark,
  strokeWidth = "1",
  fill = "none",
  variants = pathVariants,
  width,
  height,
  position,
  top,
  left,
  right,
  bottom,
  scale,
  cursor
}: Props) => {
  return (
    <>
      <div>
        <motion.svg
          className={className}
          onClick={handleClick}
          style={{
            width,
            height,
            position,
            top,
            left,
            right,
            bottom,
            scale,
            cursor
          }}
          width="5.51111in"
          height="4.44444in"
          viewBox="0 0 496 400"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          variants={svgVariants}
          initial="hidden"
          animate="visible"
        >
          <motion.path
            fill={fill}
            stroke={stroke}
            stroke-width={strokeWidth}
            variants={variants}
            d={path}
          />
        </motion.svg>
      </div>
    </>
  );
};

export default AnimatedSvg;
