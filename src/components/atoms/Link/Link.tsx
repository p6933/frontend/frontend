import { NavLink } from "react-router-dom";
import styles from "./link.module.scss";

type Props = {
  text: string;
  path: string;
};

const Link = ({ text, path }: Props) => {
  return (
    <NavLink to={path} className={styles.link}>
      {text}
    </NavLink>
  );
};

export default Link;
