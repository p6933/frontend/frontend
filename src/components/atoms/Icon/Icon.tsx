import styles from "./icon.module.scss";

type Props = {
  icon: any;
  resize?: string;
  cursor?: string;
  handleClick?: any;
  position?: "static" | "relative" | "absolute" | "sticky" | "fixed";
  top?: string;
  right?: string;
  left?: string;
  bottom?: string;
  margin?: string;
  padding?: string;
  className?: string;
};

const Icon = ({
  icon,
  resize = "20px",
  cursor,
  handleClick,
  position,
  top,
  left,
  right,
  bottom,
  margin,
  className = styles.iconWrapper,
  padding
}: Props) => {
  return (
    <div
      className={className}
      style={{
        fontSize: resize,
        cursor,
        position,
        top,
        bottom,
        left,
        right,
        margin,
        padding
      }}
      onClick={handleClick}
    >
      {icon}
    </div>
  );
};

export default Icon;
