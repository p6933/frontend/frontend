import { useEffect, useId, useState } from "react";
import { Field } from "react-final-form";
import { EnumColors } from "../../../types/enumsColors";
import styles from "./input-select-option.module.scss";

type Props = {
  name: string;
  value?: any;
  defaultValue?: any;
  placeholder?: string;
  data: any[];
  margin?: string;
  caretColor?: string;
  textColor?: string;
  type?: string;
  component?: "select" | "textarea" | "input";
  border?: string;
  borderRadius?: string;
  hasError?: boolean;
  autoFocus?: boolean;
  width?: string;
  height?: string;
  alignSelf?: string;
  datatype?: "string-array" | "object-v1-list" | "object-v2-list";
};

const InputSelectOption = ({
  data,
  defaultValue,
  name,
  alignSelf,
  autoFocus,
  border,
  borderRadius,
  caretColor,
  hasError,
  height,
  margin,
  textColor,
  type,
  width,
  placeholder,
  component,
  value,
  datatype = "string-array",
}: Props) => {
  const id = useId();

  const [toggleOptions, setToggleOptions] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<any>(value);
  const isValueInOptions = data.find((val) => val === value);
  if (value && !isValueInOptions) {
    data.unshift(value);
  }

  // css properties to handle error case
  const [properties, setProperties] = useState({
    caret: caretColor,
    color: textColor,
    borderP: border,
  });
  const errorCase = () => {
    caretColor = EnumColors.error;
    setProperties({
      caret: EnumColors.error,
      color: EnumColors.error,
      borderP: `2px solid ${EnumColors.error}`,
    });
    border = `2px solid ${EnumColors.error}`;
  };

  useEffect(() => {
    hasError && errorCase();
  }, [hasError]);

  const { caret, borderP, color } = properties;

  const selectOption = (option: any) => {
    console.log(option);
    setInputValue(option);
    console.log(inputValue);
    setToggleOptions(false);
  };

  const sortedData = data.sort((a, b) => {
    if (parseInt(a) < parseInt(b)) {
      return -1;
    }
    if (parseInt(a) > parseInt(b)) {
      return 1;
    }
    return 0;
  });

  const sortedDataV1 = data.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });

  return (
    <Field name={name} component="select" id={id}>
      <option className={styles.optionMain} style={{ background: color }}>
        {inputValue}
      </option>
      {datatype === "string-array" &&
        sortedData.map((option, index) => {
          return (
            <option
              className={styles.option}
              style={{ borderBottom: border, color }}
              onClick={() => selectOption(option)}
              key={index}
            >
              {option}
            </option>
          );
        })}
      {datatype === "object-v1-list" &&
        sortedDataV1.map((option, index) => {
          return (
            <>
              <option
                className={styles.option}
                style={{ borderBottom: border, color }}
                onClick={() => selectOption(option.id)}
                key={index}
                value={option.id}
              >
                {option.name}
              </option>
            </>
          );
        })}
    </Field>
  );
};

export default InputSelectOption;
