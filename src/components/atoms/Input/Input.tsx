import { useEffect, useState } from "react";
import { Field } from "react-final-form";
import { EnumColors } from "../../../types/enumsColors";
import styles from "./input.module.scss";

type Props = {
  name: string;
  placeholder?: string;
  margin?: string;
  caretColor?: string;
  textColor?: string;
  type?: string;
  border?: string;
  borderRadius?: string;
  hasError?: boolean;
  autoFocus?: boolean;
  width?: string;
  height?: string;
  alignSelf?: string;
};

const Input = ({
  border,
  borderRadius,
  caretColor,
  margin,
  type,
  name,
  hasError,
  textColor,
  autoFocus,
  width,
  height,
  alignSelf,
  placeholder,
}: Props) => {
  // css properties to handle error case
  const [properties, setProperties] = useState({
    caret: caretColor,
    color: textColor,
    borderP: border,
  });
  const errorCase = () => {
    caretColor = EnumColors.error;
    setProperties({
      caret: EnumColors.error,
      color: EnumColors.error,
      borderP: `2px solid ${EnumColors.error}`,
    });
    border = `2px solid ${EnumColors.error}`;
  };
  useEffect(() => {
    hasError && errorCase();
  }, [hasError]);

  const { caret, borderP, color } = properties;
  return (
    <Field name={name} type={type}>
      {(formProps) => (
        <div>
          <input
            placeholder={placeholder}
            className={styles.input}
            {...formProps.input}
            autoFocus={autoFocus}
            style={{
              border: borderP,
              borderRadius,
              caretColor: caret,
              margin,
              color,
              width,
              height,
              alignSelf,
            }}
          />
        </div>
      )}
    </Field>
  );
};

export default Input;
