import { useEffect, useState } from "react";
import LoadingSpinner from "../LoadingSpinner/LoadingSpinner";
import Logo from "../Logo/Logo";

type Props = {
  images: any[];
  type?: "images" | "component";
  resize?: string;
  margin?: string;
};

/**
 *
 * @param images list of images that one will be randomly selected
 * @param type type of images, it can be component or an image
 * @returns
 */
const RandomImage = ({ images, type = "images", resize, margin }: Props) => {
  const [pickedImage, setPickedImage] = useState();
  // select one image randomly in @param images
  const shuffleImg = (images: any[]) => {
    const shuffle = Math.floor(Math.random() * images.length);
    setPickedImage(images[shuffle]);
  };

  useEffect(() => {
    shuffleImg(images);
  }, [images]);

  return (
    <>
      {type === "images" &&
        (pickedImage ? (
          <Logo src={pickedImage} resize={resize} margin={margin} />
        ) : (
          <LoadingSpinner />
        ))}
      {type === "component" && (pickedImage ? pickedImage : <LoadingSpinner />)}
    </>
  );
};

export default RandomImage;
