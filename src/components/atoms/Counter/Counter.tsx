import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../Text/Text";
import styles from "./counter.module.scss";
import { useMediaQuery } from "react-responsive";

type CounterProps = {
  alertsCount: number;
  alertTitle: string;
};

const Counter = ({ alertsCount, alertTitle }: CounterProps) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const responsiveTextSize = !isTabletOrMobile
    ? EnumTextSize.fsXl
    : EnumTextSize.fsMedium;
  const responsiveTextSizeAlert = !isTabletOrMobile
    ? EnumTextSize.fsBig
    : EnumTextSize.fsNormal;
  return (
    <div className={styles.alerts}>
      <Text
        value={`${alertsCount}`}
        size={responsiveTextSize}
        color={EnumColors.light}
        margin="0 5px "
      />
      <Text
        value={alertTitle}
        size={responsiveTextSizeAlert}
        color={EnumColors.light}
      />
    </div>
  );
};

export default Counter;
