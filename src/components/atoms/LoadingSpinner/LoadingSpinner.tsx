import { motion } from "framer-motion";
import { loaderVariants } from "../../../animations";
import { EnumColors } from "../../../types/enumsColors";
import Text from "../Text/Text";
import styles from "./loading-spinner.module.scss";

type Props = {};

const LoadingSpinner = (props: Props) => {
  return (
    <div className={styles.loadingContainer}>
      <motion.span
        className={styles.loading}
        variants={loaderVariants}
        animate="animationFive"
        initial={{ opacity: 0.5 }}
      >
        IIIIIIIOIIIIIII
      </motion.span>
      <Text value={"Loading..."} color={EnumColors.primary} weight="bold" />
    </div>
  );
};

export default LoadingSpinner;
