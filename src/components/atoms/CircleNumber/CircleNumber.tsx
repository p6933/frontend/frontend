import styles from "./circle-number.module.scss";

type Props = {
  value: number;
  handleClick?: any;
};

const CircleNumber = ({ value, handleClick }: Props) => {
  return (
    <div onClick={handleClick} className={styles.circleNumber}>
      {value}
    </div>
  );
};

export default CircleNumber;
