import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import styles from "./text.module.scss";

type TextProps = {
  color?: EnumColors;
  size?: EnumTextSize;
  value?: string | undefined | null;
  weight?: string;
  padding?: string;
  margin?: string;
  fontStyle?: string;
  width?: string;
  align?: "end" | "left" | "right" | "center" | "justify" | "match-parent";
  whiteSpace?:
    | "normal"
    | "pre"
    | "nowrap"
    | "pre-wrap"
    | "pre-line"
    | "break-spaces";
  cursor?: string;
  handleClick?: any;
};

const Text = ({
  color = EnumColors.dark,
  size = EnumTextSize.fsNormal,
  value,
  weight,
  padding,
  margin = "0",
  fontStyle,
  width,
  align = "center",
  cursor,
  whiteSpace = "nowrap",
  handleClick,
}: TextProps) => {
  return (
    <div
      className={styles.text}
      style={{
        fontSize: size,
        color: color,
        fontWeight: weight,
        margin,
        fontStyle,
        width,
        textAlign: align,
        cursor,
        whiteSpace: whiteSpace,
        padding,
      }}
      onClick={handleClick}
    >
      {value}
    </div>
  );
};

export default Text;
