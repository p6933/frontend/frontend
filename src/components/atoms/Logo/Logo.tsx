import styles from "./logo.module.scss";
import DefaultLogo from '../../../images/logo-e-school-primary.svg'

type Props = {
  src?: any;
  resize?: string;
  margin?: string;
};

const Logo = ({ src = DefaultLogo, resize = "180px", margin = '0' }: Props) => {
  return (
    <img
      className={styles.logo}
      src={src}
      alt="logo"
      style={{ width: resize, height: resize, margin }}
    />
  );
};

export default Logo;
