import { EnumColors } from "../../../types/enumsColors";
import styles from "./line.module.scss";

type Props = {
  background?: string;
  margin?: string;
  width?: string;
  height?: string;
};

const Line = ({ background = EnumColors.primary, margin, width, height }: Props) => {
  return (
    <div className={styles.line} style={{ background, margin, width, height }}></div>
  );
};

export default Line;
