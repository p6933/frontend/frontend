import styles from "./white-card.module.scss";

type Props = {
  children: any;
};

const WhiteCard = ({ children }: Props) => {
  return <div className={styles.whiteCard}>{children}</div>;
};

export default WhiteCard;
