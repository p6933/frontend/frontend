import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../Text/Text";
import styles from "./bubble-text.module.scss";

type Props = {
  value?: string;
  children?: any;
  color?: EnumColors;
  fontStyle?: string;
  weight?: string;
  textSize?: EnumTextSize;
  textWidth?: string;
  margin?: string;
  height?: string;
  width?: string;
  border?: string;
  background?: string;
  cursor?: string;
  handleClick?: any;
  whiteSpace?:
    | "normal"
    | "pre"
    | "nowrap"
    | "pre-wrap"
    | "pre-line"
    | "break-spaces";
};

const BubbleText = ({
  value,
  color = EnumColors.light,
  fontStyle = "italic",
  margin,
  width,
  height,
  textSize,
  textWidth,
  background,
  weight = "bold",
  border,
  cursor,
  handleClick,
  whiteSpace = "nowrap",
  children,
}: Props) => {
  return (
    <div
      className={styles.bubbleText}
      style={{
        margin,
        width,
        height,
        background,
        border,
        cursor,
        whiteSpace,
      }}
      onClick={handleClick}
    >
      {!children ? (
        <Text
          value={value}
          color={color}
          fontStyle={fontStyle}
          weight={weight}
          size={textSize}
          width={textWidth}
        />
      ) : (
        children
      )}
    </div>
  );
};

export default BubbleText;
