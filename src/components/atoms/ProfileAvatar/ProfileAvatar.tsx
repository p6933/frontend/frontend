import styles from "./profile-avatar.module.scss";
import Avatar from "../../../images/avatar2.png";

type Props = {
  src?: any;
  handleClick?: any;
  resize?: string;
  border?: string;
  background?: string;
  position?: "static" | "relative" | "absolute" | "sticky" | "fixed";
  top?: string;
  left?: string;
  right?: string;
  bottom?: string;
  margin?: string;
};

const ProfileAvatar = ({
  src = Avatar,
  handleClick,
  resize = "40px",
  background = "none",
  border = "2px solid black",
  position,
  bottom,
  left,
  right,
  top,
  margin,
}: Props) => {
  if (src === null) {
    src = Avatar;
  }
  return (
    <div
      className={styles.avatarWrapper}
      style={{
        width: resize,
        height: resize,
        background,
        border,
        position,
        left,
        top,
        right,
        bottom,
        margin,
      }}
    >
      <img
        className={styles.profileAvatar}
        src={src}
        onClick={handleClick}
        alt="avatar"
      ></img>
    </div>
  );
};

export default ProfileAvatar;
