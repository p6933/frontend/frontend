import { createContext, useReducer } from "react";
import useAuthCtx from "../../hooks/useAuthCtx";
import useToken from "../../hooks/useToken";

interface AuthContextInterface {
  state: any;
  dispatch: (action: any) => void;
}

const AuthContext = createContext({} as AuthContextInterface);

const authReducer = (state: any, action: any) => {
  switch (action.type) {
    case "LOGIN":
      console.log(action.payload);
      return {
        ...state,
        currentUser: action.payload.currentUser,
        userId: action.payload.userId,
        isLoggedIn: action.payload.isLoggedIn,
        userType: action.payload.userType,
      };
    case "LOGOUT":
      return {
        ...state,
        currentUser: {},
        isLoggedIn: false,
        userId: null,
        userType: null,
      };
    default:
      break;
  }
};

type Props = {
  children: any;
};

const AuthContextProvider = ({ children }: Props) => {
  // useToken methods
  const { token, setToken, tokenClear, tokenProfileInfos } = useToken();

  const { logout, login } = useAuthCtx();

  const startValues: any = {
    login: login,
    logout: logout,
    currentUser: tokenProfileInfos,
    isLoggedIn: false,
    userId: null,
    userType: null,
    token: token,
  };
  const [state, dispatch] = useReducer(authReducer, startValues);
  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContextProvider, AuthContext };
