import styles from "./box.module.scss";

type Props = {
  children: any;
  width?: string;
  height?: string;
  border?: string;
  minHeight?: string;
  minWidth?: string;
  justifyContent?: string;
  padding?: string;
  margin?: string;
  aos?: string;
};

const Box = ({
  children,
  width,
  height,
  border,
  minHeight,
  minWidth,
  justifyContent,
  padding,
  margin,
  aos,
}: Props) => {
  return (
    <div
      data-aos={aos}
      className={styles.box}
      style={{
        width,
        height,
        border,
        minHeight,
        minWidth,
        justifyContent,
        padding,
        margin,
      }}
    >
      {children}
    </div>
  );
};

export default Box;
