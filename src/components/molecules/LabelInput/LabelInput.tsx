import { ReactNode } from "react";
import styles from "./label-input.module.scss";

type Props = {
  label: ReactNode;
  children: ReactNode;
};

const LabelInput = ({ label, children }: Props) => {
  return (
    <div className={styles.labelInput}>
      {label}
      {children}
    </div>
  );
};

export default LabelInput;
