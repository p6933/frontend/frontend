import { EnumTextSize } from "../../../types/enumsSizes";
import Logo from "../../atoms/Logo/Logo";
import Text from "../../atoms/Text/Text";
import styles from "./notfound.module.scss";

const Notfound = () => {
  return (
    <div className={styles.notfound}>
      <Text
        value="4"
        size={EnumTextSize.fsGiant}
        fontStyle="italic"
        weight="bold"
      />
      <Logo margin="0 10px 0 30px " />
      <Text
        value="4"
        size={EnumTextSize.fsGiant}
        fontStyle="italic"
        weight="bold"
      />
    </div>
  );
};

export default Notfound;
