import { EnumColors } from "../../../types/enumsColors";
import ProfileAvatar from "../../atoms/ProfileAvatar/ProfileAvatar";
import Text from "../../atoms/Text/Text";
import Flex from "../../organisms/wrapper/Flex";
import Grid3 from "../../organisms/wrapper/Grid3";
import SingleInputForm from "../SingleInputForm/SingleInputForm";
import TextUnderline from "../TextUnderline/TextUnderline";
import { useMediaQuery } from "react-responsive";
import { EnumTextSize } from "../../../types/enumsSizes";

type Props = {
  label: string;
  value?: string | undefined | null;
  name: string;
  isSrc?: boolean;
  type?: any;
  onSubmit?: any;
  hasError?: any;
  modify?: any;
};

const Infos = ({
  label,
  value,
  onSubmit,
  name,
  hasError,
  modify,
  isSrc,
  type,
}: Props) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 750px)" });
  const isMobile = useMediaQuery({ query: "(max-width: 500px)" });
  const responsiveTextSize = !isTabletOrMobile
    ? EnumTextSize.fsMedium
    : EnumTextSize.fsNormal;
  const responsiveDirection = !isMobile ? "row" : "column";
  return (
    <>
      {modify ? (
        <Grid3>
          <TextUnderline
            fontSize={responsiveTextSize}
            text={label}
            textWeight="bold"
          />
          <SingleInputForm
            name={name}
            onSubmit={onSubmit}
            hasError={hasError}
            type={type}
          />
          {!isSrc ? (
            <Text value={value} weight="bold" color={EnumColors.primary} />
          ) : (
            <ProfileAvatar src={value} border="none" />
          )}
        </Grid3>
      ) : (
        <Flex flexDirection={responsiveDirection} padding="10px 40px">
          <TextUnderline text={label} textWeight="bold" lineHeight="5px" />
          {!isSrc ? (
            <Text value={value} weight="bold" color={EnumColors.primary} />
          ) : (
            <ProfileAvatar resize="100px" src={value} border="none" />
          )}
        </Flex>
      )}
    </>
  );
};

export default Infos;
