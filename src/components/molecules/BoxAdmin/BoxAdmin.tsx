import { EnumColors } from "../../../types/enumsColors";
import Text from "../../atoms/Text/Text";
import { EnumTextSize } from "../../../types/enumsSizes";

import styles from "./box-admin.module.scss";

type Props = {
  title: string;
  handleClick?: any;
  fontSize?: EnumTextSize;
  aos?: string;
};

const BoxAdmin = ({
  title,
  handleClick,
  fontSize,
  aos = "fade-out",
}: Props) => {
  return (
    <div className={styles.boxAdmin} onClick={handleClick} data-aos={aos}>
      <Text value={title} color={EnumColors.light} size={fontSize} />
    </div>
  );
};

export default BoxAdmin;
