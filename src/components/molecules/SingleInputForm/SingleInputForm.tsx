import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import FormContainer from "../../organisms/FormContainer/FormContainer";
import Flex from "../../organisms/wrapper/Flex";
import { useMediaQuery } from "react-responsive";

type Props = {
  name: string;
  onSubmit?: any;
  hasError?: any;
  type?: any;
};

const SingleInputForm = ({
  onSubmit,
  name,
  hasError,
  type = "text",
}: Props) => {
  const isMobile = useMediaQuery({ query: "(max-width: 460px)" });
  const responsiveFlexSize = !isMobile ? "100%" : "80%";
  return (
    <FormContainer onSubmit={onSubmit}>
      <Flex width={responsiveFlexSize}>
        <Input
          name={name}
          hasError={hasError}
          border="2px solid #9166FF"
          textColor={EnumColors.primary}
          type={type}
        />
        <Button
          text="Entrer"
          type="submit"
          height="20px"
          margin="10px"
          fontSize={EnumTextSize.fsNormal}
        />
      </Flex>
    </FormContainer>
  );
};

export default SingleInputForm;
