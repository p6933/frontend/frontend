import Input from '../../atoms/Input/Input'
import Text from '../../atoms/Text/Text'
import styles from './search-bar.module.scss'

type Props = {
    value:string,
    name:string
    
}

const SearchBar = ({value,name}: Props) => {
  return (
    <div className={styles.searchBar} >

        <Text fontStyle='italic' value={value} margin="10px" weight='bold'/>
        <Input name={name}/>
        
    </div>
  )
}

export default SearchBar