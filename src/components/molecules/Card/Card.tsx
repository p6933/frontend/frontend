import { EnumColors } from "../../../types/enumsColors";
import { MyFeature } from "../../../types/myfeature";
import Counter from "../../atoms/Counter/Counter";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import CardBox from "../CardBox/CardBox";
import styles from "./card.module.scss";
import { MdAddAlert } from "react-icons/md";
import IconProvider from "../../providers/IconProvider";
import { EnumTextSize } from "../../../types/enumsSizes";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from 'react-responsive'

type Props = {
  feature: MyFeature;
};

const Card = ({ feature }: Props) => {
  const navigate = useNavigate();
  const { appname, src, alerts, link } = feature;
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })
  const responsiveTextSize = !isTabletOrMobile ? EnumTextSize.fsXl : EnumTextSize.fsMedium
  return (
    <div className={styles.card}>
      <IconProvider color={EnumColors.light}>
        <CardBox borderBottom="1px solid #fff" height="20%">
          <Text
            cursor="pointer"
            handleClick={() => navigate(link)}
            value={appname}
            color={EnumColors.light}
            size={responsiveTextSize}
            whiteSpace="normal"
          />
        </CardBox>
        <CardBox height="60%">
          <img
            style={{ width: "100%", height: "100%", cursor:'pointer' }}
            src={src}
            alt="src"
            onClick={() => navigate(link)}
          />
        </CardBox>
        <CardBox borderTop="1px solid #fff" height="20%">
          <Icon
            handleClick={() => navigate(link)}
            icon={<MdAddAlert />}
            cursor="pointer"
          />
          <Counter
            alertsCount={alerts.alertCount}
            alertTitle={alerts.alertTitle}
          />
        </CardBox>
      </IconProvider>
    </div>
  );
};

export default Card;
