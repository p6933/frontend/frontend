import styles from "./card-box.module.scss";

type Props = {
  children: any;
  borderTop?: string;
  borderBottom?: string;
  height?: string
};

const CardBox = ({ children, borderTop, borderBottom, height }: Props) => {
  return (
    <div className={styles.cardbox} style={{ borderTop, borderBottom, height }}>
      {children}
    </div>
  );
};

export default CardBox;
