import Icon from "../../atoms/Icon/Icon";
import styles from "./headband.module.scss";
import { BsArrowRight as ArrowR } from "react-icons/bs";
import { BsArrowLeft as ArrowL } from "react-icons/bs";
import Text from "../../atoms/Text/Text";
import { EnumColors } from "../../../types/enumsColors";
import IconProvider from "../../providers/IconProvider";
type Props = {};

const HeadBand = (props: Props) => {
  return (
    <div className={styles.headband}>
      <IconProvider color={EnumColors.light}>
        <Icon icon={<ArrowL />} cursor="pointer" />
        <Text
          value="Infos de la semaine"
          color={EnumColors.light}
          margin={"0 5px"}
        />
        <Icon icon={<ArrowR />} cursor="pointer" />
      </IconProvider>
    </div>
  );
};

export default HeadBand;
