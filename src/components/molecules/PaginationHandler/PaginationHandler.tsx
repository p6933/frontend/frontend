import { EnumTextSize } from "../../../types/enumsSizes";
import Icon from "../../atoms/Icon/Icon";
import Text from "../../atoms/Text/Text";
import styles from "./pagination-handler.module.scss";
import {
  IoCaretBackCircleSharp,
  IoCaretForwardCircleSharp,
} from "react-icons/io5";
import IconProvider from "../../providers/IconProvider";
import { EnumColors } from "../../../types/enumsColors";

type Props = {
  page: number;
  next: () => void;
  prev: () => void;
};

const PaginationHandler = ({ next, page, prev }: Props) => {
  return (
    <div className={styles.wrapperPagination}>
      <IconProvider color={EnumColors.primary}>
        <Icon
          icon={<IoCaretBackCircleSharp />}
          cursor="pointer"
          handleClick={prev}
          resize="40px"
          padding="20px"
        />
        <Text
          value={`${page}`}
          fontStyle="italic"
          padding="20px"
          size={EnumTextSize.fsBig}
          color={EnumColors.primary}
        />
        <Icon
          icon={<IoCaretForwardCircleSharp />}
          handleClick={next}
          resize="40px"
          padding="20px"
          cursor="pointer"
        />
      </IconProvider>
    </div>
  );
};

export default PaginationHandler;
