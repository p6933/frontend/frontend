import React from "react";
import { EnumTextSize } from "../../../types/enumsSizes";
import Text from "../../atoms/Text/Text";
import Flex from "../../organisms/wrapper/Flex";
import TextUnderline from "../TextUnderline/TextUnderline";

type Props = {
  studentClick: any;
  profClick: any;
  accountType: "student" | "prof";
};

const LoginHeader = ({ accountType, profClick, studentClick }: Props) => {
  return (
    <Flex margin="20px" width="250px">
      {accountType === "student" && (
        <>
          <TextUnderline
            text="Etudiant"
            handleClick={studentClick}
            margin="8px 0 0 0"
            fontSize={EnumTextSize.fsBig}
            lineWidth="70px"
            cursor="pointer"
          />
          <Text
            value="Professeur"
            handleClick={profClick}
            size={EnumTextSize.fsBig}
            cursor="pointer"
          />
        </>
      )}
      {accountType === "prof" && (
        <>
          <Text
            value="Etudiant"
            handleClick={studentClick}
            size={EnumTextSize.fsBig}
            cursor="pointer"
          />
          <TextUnderline
            text="Professeur"
            handleClick={profClick}
            fontSize={EnumTextSize.fsBig}
            cursor="pointer"
            margin="8px 0 0 0"
            lineWidth="70px"
          />
        </>
      )}
    </Flex>
  );
};

export default LoginHeader;
