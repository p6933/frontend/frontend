import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import Line from "../../atoms/Line/Line";
import Text from "../../atoms/Text/Text";
import styles from "./text-underline.module.scss";

type Props = {
  text: string;
  fontSize?: EnumTextSize;
  textWeight?: string;
  textWidth?: string;
  textColor?: EnumColors;
  textAlign?:
    | "end"
    | "left"
    | "right"
    | "center"
    | "justify"
    | "match-parent"
    | undefined;
  lineHeight?: string;
  lineWidth?: string;
  lineColor?: string;
  margin?: string;
  lineMargin?: string;
  fontStyle?: string;
  textMargin?: string;
  alignItems?: string;
  flexDirection?: "column" | "column-reverse" | "row" | "row-reverse";
  cursor?: any;
  handleClick?: any;
};

const TextUnderline = ({
  text,
  fontSize = EnumTextSize.fsMedium,
  textWidth,
  textWeight,
  textColor,
  textAlign,
  lineHeight = "3px",
  lineWidth = "30px",
  lineColor,
  lineMargin = "5px 0 0 5px",
  alignItems,
  margin,
  fontStyle,
  textMargin,
  flexDirection,
  handleClick,
  cursor
}: Props) => {
  return (
    <div
      className={styles.textUnderline}
      style={{ margin, flexDirection, alignItems }}
    >
      <Text
        value={text}
        size={fontSize}
        align={textAlign}
        color={textColor}
        weight={textWeight}
        width={textWidth}
        fontStyle={fontStyle}
        margin={textMargin}
        handleClick={handleClick}
        cursor={cursor}
      />
      <Line
        width={lineWidth}
        height={lineHeight}
        margin={lineMargin}
        background={lineColor}
      />
    </div>
  );
};

export default TextUnderline;
