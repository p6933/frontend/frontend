import styles from "./navlink.module.scss";

type Props = {
  children: any;
  cursor?: string;
  handleClick?: any;
  width?: string;
};

const Navlink = ({ children, cursor, handleClick, width }: Props) => {
  return (
    <div className={styles.navlink} style={{ cursor, justifyContent: 'start', width }} onClick={handleClick}>
      {children}
    </div>
  );
};

export default Navlink;
