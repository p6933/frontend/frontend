import Logo from "../../atoms/Logo/Logo";
import Text from "../../atoms/Text/Text";
import styles from "./navlogo.module.scss";
import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import ImageLogo from "../../../images/logo-e-school.svg";

type Props = {
  handleClick?: any;
};

const Navlogo = ({ handleClick }: Props) => {
  return (
    <div className={styles.navlogo} onClick={handleClick}>
      <Text
        value="E-School"
        color={EnumColors.light}
        size={EnumTextSize.fsBig}
      />
      <Logo src={ImageLogo} resize="40px" />
    </div>
  );
};

export default Navlogo;
