import { useEffect, useState } from "react";
import { Field } from "react-final-form";
import { EnumColors } from "../../../types/enumsColors";
import styles from "./input-dropdown.module.scss";

type Props = {
  name: string;
  value?: any;
  defaultValue?: any;
  placeholder?: string;
  data: any[];
  margin?: string;
  caretColor?: string;
  textColor?: string;
  type?: string;
  component?: "select" | "textarea" | "input";
  border?: string;
  borderRadius?: string;
  hasError?: boolean;
  autoFocus?: boolean;
  width?: string;
  height?: string;
  alignSelf?: string;
};

const InputDropdown = ({
  data,
  defaultValue,
  name,
  alignSelf,
  autoFocus,
  border,
  borderRadius,
  caretColor,
  hasError,
  height,
  margin,
  textColor,
  type,
  width,
  placeholder,
  component,
  value,
}: Props) => {
  const [toggleOptions, setToggleOptions] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<any>(value);
  const isValueInOptions = data.find((val) => val === value);
  if (value && !isValueInOptions) {
    data.unshift(value);
  }

  // css properties to handle error case
  const [properties, setProperties] = useState({
    caret: caretColor,
    color: textColor,
    borderP: border,
  });
  const errorCase = () => {
    caretColor = EnumColors.error;
    setProperties({
      caret: EnumColors.error,
      color: EnumColors.error,
      borderP: `2px solid ${EnumColors.error}`,
    });
    border = `2px solid ${EnumColors.error}`;
  };


  useEffect(() => {
    hasError && errorCase();
  }, [hasError]);

  const { caret, borderP, color } = properties;

  const selectOption = (option: any) => {
    setInputValue(option);
    setToggleOptions(false);
  };

  return (
    <>
      <Field name={name} component="select">
              <option className={styles.optionMain} style={{background: color}}>{value}</option>
              {data.map((option, index) => {
                  return (
                    <option
                      className={styles.option}
                      style={{ borderBottom: border, color }}
                      onClick={() => selectOption(option)}
                      key={index}
                    >
                      {option}
                    </option>
                  );
                })}
            </Field>
    </>
  );
};

export default InputDropdown;
