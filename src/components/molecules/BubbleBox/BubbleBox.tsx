import { EnumColors } from "../../../types/enumsColors";
import { EnumTextSize } from "../../../types/enumsSizes";
import CircleNumber from "../../atoms/CircleNumber/CircleNumber";
import Text from "../../atoms/Text/Text";
import styles from "./bubble-box.module.scss";

type Props = {
  value: string;
  color?: EnumColors;
  fontStyle?: string;
  weight?: string;
  textSize?: EnumTextSize;
  textWidth?: string;
  margin?: string;
  height?: string;
  width?: string;
  border?: string;
  background?: string;
  cursor?: string;
  handleClick?: any;
  textAction?: any;
  circleAction?: any;
  note?: any;
};

const BubbleBox = ({
  value,
  color = EnumColors.dark,
  fontStyle = "italic",
  margin,
  width,
  height,
  textSize,
  textWidth,
  background,
  weight = "bold",
  border,
  cursor,
  circleAction,
  textAction,
  note,
}: Props) => {
  return (
    <div
      className={styles.bubbleBox}
      style={{ margin, width, height, background, border, cursor }}
    >
      <Text
        value={value}
        color={color}
        fontStyle={fontStyle}
        weight={weight}
        size={textSize}
        width={textWidth}
        handleClick={textAction}
      />
      {note && <CircleNumber value={note} handleClick={circleAction} />}
    </div>
  );
};

export default BubbleBox;
