import { useCallback } from "react";
import useToken from "./useToken";
import Toaster from "../components/atoms/Toaster/Toaster";
import { render } from "@testing-library/react";

export default function useAuthCtx() {
  // useToken methods
  const { token, setToken, tokenClear } = useToken();

  const logout = useCallback((e: any) => {
    if (e) {
      console.warn(e);
    }
    // tokenClear();
    if (token) {
      tokenClear()
    }
    render(<Toaster info toastMsg="Logged out !" id={"logout"} />);
  }, []);

  const login = useCallback((loginToken: any) => {
    try {
      setToken(loginToken);
    } catch (e) {
      logout(e);
    }
  }, []);

  return {
    logout,
    login,
  };
}
